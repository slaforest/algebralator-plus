//
//  QuadraticFormulaViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"


@class DetailViewController;

@interface QuadraticFormulaViewController : UIViewController <UITextFieldDelegate>
{
    
    

@private
	UITextField *aTextField;
	UITextField *bTextField;
	UITextField *cTextField;
	UITextView *answerTextView;
	UITextField *activeField;
    
	double a;
	double b;
	double c;
    double xPlus;
    double xMin;
    NSString *answerString;

}

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *aTextField;
@property (nonatomic, retain) IBOutlet UITextField *bTextField;
@property (nonatomic, retain) IBOutlet UITextField *cTextField;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property double discriminant;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain)IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;

- (void) createContentPages;

- (IBAction) completeSquareButtonPressed: (id) sender;
-(IBAction)clearButtonPressed:(id)sender;
@end

//
//  ParallelLinesViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "ParallelLinesViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation ParallelLinesViewController
@synthesize instructionsText, answerTextView, directionsTextView, mTextField, bTextField, xTextField, yTextField, m, b, x, y, lineType, variable1Label, variable2Label, navigationBar,scrollView, detailViewController, summaryViewController , mainDetailViewController, showWork;



- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2*activeField.frame.origin.y);//-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}
// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}



-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   

    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
	[sender resignFirstResponder];	
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar c = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self registerForKeyboardNotifications];
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    showWork = [[UIBarButtonItem alloc] 
                initWithTitle:@"Show Work"                                            
                style:UIBarButtonItemStyleBordered 
                target:self 
                action:@selector(showWorkView)];
    


    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    self.xTextField.delegate = self;
    self.yTextField.delegate = self;
    self.mTextField.delegate = self;
    self.bTextField.delegate = self;

    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]); 
    
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
}





- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}

-(IBAction)clearButtonPressed:(id)sender{
    
    mTextField.text = @"";
    bTextField.text = @"";
    xTextField.text = @"";
    yTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];

    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

-(IBAction) calculateButtonPressed: (id) sender{
	self.m = [mTextField.text doubleValue];
	self.b = [bTextField.text doubleValue];
	self.x = [xTextField.text doubleValue];
	self.y = [yTextField.text doubleValue];
	
	
    if(lineType.selectedSegmentIndex == 0){
        answerTextView.text = [NSString stringWithFormat:@"y = %g  ", y];
    }
    else if(lineType.selectedSegmentIndex == 1){
        answerTextView.text = [NSString stringWithFormat:@"x = %g  ", x];
        }else{
                newB = y - m*x;
                newM = m;
            if (newB == 0) {
                answerTextView.text = [NSString stringWithFormat:@"y = %g x ", newM];
            }else if(newM == 0){//same as selectedSegmentIndex == 0
                answerTextView.text = [NSString stringWithFormat:@"y = %g", newB];	
            }else {
                answerTextView.text = [NSString stringWithFormat:@"y = %g x + %g",newM, newB];	
            }
    }
    NSString *answerText = answerTextView.text;
    
    if(lineType.selectedSegmentIndex == 0){
        answerString = [NSString stringWithFormat: @"<html><style> table.workline {width:95%%; } td.work {width: 95%%; text-align:left; }td.numbering {text-align: right; } td.space {width:45%%; text-align:right;  }td.answer {border:solid 2px red; }</style><p> Since parallel lines have the same slopes, the slope of the new line is</p><table class='workline'><tr><td class = 'work'> m = 0 </td><td class = 'numbering'> (1)</td></tr></table><p> Since the new line has to go through the point (%g, %g) and have a slope of 0. Our new line is a horizontal line at: </p><table><tr><td class='answer'>%@ </td></tr></table></html",x,y, answerText];
    }
    else if(lineType.selectedSegmentIndex == 1){
        answerString = [NSString stringWithFormat: @"<html><style> table.workline {width:95%%; } td.work {width: 95%%; text-align:left; }td.numbering {text-align: right; } td.space {width:45%%; text-align:right;  }td.answer {border:solid 2px red; }</style><p> Since parallel lines have the same slopes, the slope of the new line is</p><table class='workline'><tr><td class = 'work'> m is undefined </td><td class = 'numbering'> (1)</td></tr></table><p> Since the new line has to go through the point (%g, %g) and have an undefined slope. Our new line is a vertical line at: </p><table><tr><td class='answer'>%@ </td></tr></table></html",x,y, answerText];    
    }else{
    answerString = [NSString stringWithFormat: @"<html><style> table.workline {width:95%%; } td.work {width: 95%%; text-align:left; }td.numbering {text-align: right; } td.space {width:45%%; text-align:right;  }td.answer {border:solid 2px red; }</style><p> Since parallel lines have the same slopes, the slope of the new line is</p><table class='workline'><tr><td class = 'work'> m = %g </td><td class = 'numbering'> (1)</td></tr></table><p> Now we can use the point slope form of an equation:</p><table class='workline'><tr><td class ='work'> y - y<sub>1</sub> = m(x - x<sub>1</sub>)</td><td class ='numbering'>(2)</td></tr></table><p>Subbing in the x<sub>1</sub>,y<sub>1</sub>, and m values:</p><table class='workline'><tr><td class ='work'> y - %g = %g(x - %g)</td><td class ='numbering'>(3)</td></tr></table><p>Solving for y you get:</p><table><tr><td class='answer'> %@</td></tr></table></html",m,y,m,x, answerText];
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = answerString;
        [mainDetailViewController.answerWebView setHidden:YES];

        [mainDetailViewController configureView];
        
    }
[self createContentPages];
}

-(IBAction) segmentedControlIndexChanged{
    switch (self.lineType.selectedSegmentIndex) {
            
        case 0://set UI for horizontal lines
            self.variable1Label.text =@"y =";
            self.variable2Label.text =@" ";
            self.mTextField.placeholder = @"";
            self.bTextField.text = @"";
            self.bTextField.hidden = YES;
            [self.view setNeedsDisplay];
            break;
            
        case 1://set UI for vertical lines
            self.variable1Label.text =@"x =";
            self.variable2Label.text =@" ";
            self.mTextField.placeholder = @"";
            self.bTextField.text = @"";
            self.bTextField.hidden = YES;
            [self.view setNeedsDisplay];

            break;
            
        case 2:
            self.variable1Label.text =@"y =";
            self.variable2Label.text =@"x +";
            self.bTextField.hidden = NO;
            [self.view setNeedsDisplay];

            break;
            
                    
    }

}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *contentString;
    if (self.lineType.selectedSegmentIndex == 0) {
        contentString = [NSString 
                                   stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to y = %g  and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, x, y,answerTextView.text];
    }else if (self.lineType.selectedSegmentIndex == 1){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to x =  %g and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, x, y,answerTextView.text];
    }else{
    contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br />Line parallel to y = %gx + %g and passing through (%g, %g) <br />%@ </h2></body></html>",dateString, m, b, x, y,answerTextView.text];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
      
        [mainDetailViewController viewWillAppear:YES];
        
        
    }
}
-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}

- (void)dealloc {
    //[super dealloc];
}


@end

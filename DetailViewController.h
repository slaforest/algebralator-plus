//
//  DetailViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>


@interface DetailViewController : UIViewController <UISplitViewControllerDelegate>
{
    AppDelegate *appDelegate;
}
//@property (nonatomic, retain) UIPageViewController *pageViewController;
//@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) id detailItem;
//@property (strong, nonatomic) NSMutableString *webViewString;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIWebView *answerWebView;

@property (strong, nonatomic) IBOutlet UIImageView *paperView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *summaryButton;
@property (nonatomic, retain)  NSString *solutionString;
@property (nonatomic, retain) NSString *answerString;
@property (nonatomic, retain) IBOutlet UITextView *detailAnswerView;




@property (strong, nonatomic) id dataObject;
-(void)resetView;
-(void)configureView;


@end

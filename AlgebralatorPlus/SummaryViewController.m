//
//  SummaryViewController.m
//  AlgebralatorPro
//
//  Created by Scott LaForest on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SummaryViewController.h"

@implementation SummaryViewController
@synthesize webView, webViewString, paperView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)clearWebView{
    [appDelegate.summaryStringsArray removeAllObjects];
    [self viewWillAppear:YES];
}
- (void)configureView
{
    
    
    
    for(int i = 0; i < [appDelegate.summaryStringsArray count] ; i++){
        [webViewString appendString:[appDelegate.summaryStringsArray objectAtIndex:i]];
        
    }
    [self.webView loadHTMLString:webViewString baseURL:nil];
    //[webViewString setString:@""];
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    [self.webViewString setString:@""];
    [self configureView];
    // }
    
    
    
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *clearButton = [[UIBarButtonItem alloc] 
                                    initWithTitle:@"Clear"                                            
                                    style:UIBarButtonItemStyleBordered 
                                    target:self 
                                    action:@selector(clearWebView)];
    self.navigationItem.rightBarButtonItem = clearButton;
    
    [[self.webView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[self.webView layer] setBorderWidth:2.3];
    [[self.webView layer] setCornerRadius:15];
    [self.webView setClipsToBounds: YES];

    
    webViewString = [[NSMutableString alloc] initWithString: @""];
    appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end

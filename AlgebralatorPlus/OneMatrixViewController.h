//
//  OneMatrixViewController.h
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 3/22/13.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class DetailViewController;

@interface OneMatrixViewController : UIViewController <UITextFieldDelegate, UISplitViewControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UIWebViewDelegate>
{
    NSArray* operationsArray;
    NSArray* sizesArray;
    UIToolbar* pickerToolbar;
    NSArray* aRow1 ;
    NSArray* aRow2 ;
    NSArray* aRow3 ;
    NSArray* acolumn1;
    NSArray* acolumn2 ;
    NSArray* acolumn3;
    UIBarButtonItem* infoButton;
    NSString* htmlString;
    NSString* solutionString;


}

@property (nonatomic, retain) IBOutlet UITextField* rowField;
@property (nonatomic, retain) IBOutlet UITextField* acolumnField;
@property (nonatomic, retain) IBOutlet UITextField* operationField;

@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UIWebView *answerWebView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField* a11;
@property (nonatomic, retain) IBOutlet UITextField* a12;
@property (nonatomic, retain) IBOutlet UITextField* a13;
@property (nonatomic, retain) IBOutlet UITextField* a21;
@property (nonatomic, retain) IBOutlet UITextField* a22;
@property (nonatomic, retain) IBOutlet UITextField* a23;
@property (nonatomic, retain) IBOutlet UITextField* a31;
@property (nonatomic, retain) IBOutlet UITextField* a32;
@property (nonatomic, retain) IBOutlet UITextField* a33;
@property(nonatomic, strong)  UITextField* activeField;

@property (nonatomic, retain) IBOutlet UIButton* calcButton;
@property (nonatomic, retain) IBOutlet UIButton* clearButton;
@property (nonatomic, retain) IBOutlet UIPickerView* sizePicker;
@property (nonatomic, retain) IBOutlet UIPickerView* operationPicker;




-(IBAction)calculateButtonPressed:(id)sender;
- (void) createContentPages;
-(IBAction)clearButtonPressed:(id)sender;

@end

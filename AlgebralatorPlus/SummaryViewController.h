//
//  SummaryViewController.h
//  AlgebralatorPro
//
//  Created by Scott LaForest on 2/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface SummaryViewController : UIViewController
{
    AppDelegate *appDelegate;
}
//@property (nonatomic, retain) UIPageViewController *pageViewController;
//@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) NSMutableString *webViewString;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIImageView *paperView;



-(void)clearWebView;
-(void)configureView;
@end

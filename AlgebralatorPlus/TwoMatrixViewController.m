//
//  TwoMatrixViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TwoMatrixViewController.h"
#import "ShowWorkViewController.h"

@interface TwoMatrixViewController ()

@end
//
//  OneMatrixViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 7/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"



@implementation TwoMatrixViewController
@synthesize scrollView, modelArray, mainDetailViewController, detailViewController,
summaryViewController, answerTextView,directionsTextView,showWork;
@synthesize rowField, acolumnField, sizePicker, operationField, operationPicker, calcButton, clearButton;
@synthesize a11,a12,a13, a14,a21,a22, a23, a24,a31,a32,a34,a33, a41,a42,a43,a44,b12
,b11, b13,b14,b21,b22,b23,b24,b31,b32,b33,b34,b41,b42,b43,b44, bRowField, bColumnField, bSizePicker;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    
    
    lastActiveField = activeField;
    activeField = textField;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    //activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        UIInterfaceOrientation orientation = self.interfaceOrientation;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,50);//- activeField.frame.origin.y) ;//-kbSize.height);
                [scrollView setContentOffset:scrollPoint animated:YES];
            }
            
        }else {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,135);//- activeField.frame.origin.y) ;//-kbSize.height);
                [scrollView setContentOffset:scrollPoint animated:YES];
            }
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    [activeField resignFirstResponder];
}

# pragma Controlling UI, user input, and keyboard
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // Show UIPickerView
    /*self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = infoButton;
    NSLog(@"shoul begin editing");
    [bSizePicker setHidden:YES];
    
    [sizePicker setHidden:YES];
    [operationPicker setHidden:YES];
    
    //[activeField resignFirstResponder];
    lastActiveField = activeField;
    activeField = textField;
    if(activeField == rowField || activeField == acolumnField){
        [calcButton setEnabled:NO];
        [clearButton setEnabled:NO];
        [sizePicker setHidden:NO];
        [bSizePicker setHidden:YES];
        [operationPicker setHidden:YES];
        [lastActiveField resignFirstResponder];
        [activeField resignFirstResponder];
        
        return NO;  
    }else if(activeField == operationField){
        [calcButton setEnabled:NO];
        [clearButton setEnabled:NO];
        [operationPicker setHidden:NO];
        [sizePicker setHidden:YES];
        [bSizePicker setHidden:YES];
        
        [lastActiveField resignFirstResponder];
        
        return NO;
    }
    else if(activeField == bRowField || activeField == bColumnField){
        [bSizePicker setHidden:NO];
        
        [operationPicker setHidden:YES];
        [sizePicker setHidden:YES];
        [lastActiveField resignFirstResponder];
        
        return NO;
    }
    
    else {
        
        [sizePicker setHidden:YES];
        [operationPicker setHidden:YES];
        
        return YES;
        
    }*/
    return YES;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    if (pickerView == sizePicker || pickerView == bSizePicker) {
        return 3;
    }else {
        return 1;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == sizePicker) {
        if (activeField == rowField || activeField == acolumnField) {
            rowField.text = [sizesArray objectAtIndex:[pickerView selectedRowInComponent:0]];
            acolumnField.text = [sizesArray objectAtIndex:[pickerView selectedRowInComponent:2]];
        }else{
            bRowField.text = [sizesArray objectAtIndex:[pickerView selectedRowInComponent:0]];
            bColumnField.text = [sizesArray objectAtIndex:[pickerView selectedRowInComponent:2]];
        }
        
        [self configureMatrix];
    
    }
    else {
        operationField.text = [operationsArray objectAtIndex:[pickerView selectedRowInComponent:0]];
        [pickerView setHidden:YES];
        [self hidePicker:self];
    }
    
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if (pickerView == sizePicker|| pickerView == bSizePicker) {
        if(component == 0 || component == 2){
            return [sizesArray count];
        }else {
            return 1;
        }
    }else {
        return [operationsArray count];
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    if (pickerView == sizePicker|| pickerView == bSizePicker) {
        
        if(component == 0 || component == 2){
            return [sizesArray objectAtIndex:row];
        }else {
            return @"X";
        }
    }else {
        return [operationsArray objectAtIndex:row];
    }
}
-(void)configureMatrix{
    
    aRow1 = [[NSArray alloc] initWithObjects:a11,a12,a13,a14, nil];
    aRow2 = [[NSArray alloc] initWithObjects:a21,a22,a23,a24, nil];
    aRow3 = [[NSArray alloc] initWithObjects:a31,a32,a33,a34, nil];
    aRow4 = [[NSArray alloc] initWithObjects:a41,a42,a43,a44, nil];
    acolumn1 = [[NSArray alloc] initWithObjects:a11,a21,a31, a41, nil];
    acolumn2 = [[NSArray alloc] initWithObjects:a12,a22,a32, a42, nil];
    acolumn3 = [[NSArray alloc] initWithObjects:a13,a23,a33, a43, nil];
    acolumn4 = [[NSArray alloc] initWithObjects:a14,a24,a34, a44, nil];
    if ([rowField.text isEqual: @"1"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:YES];
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[aRow4 objectAtIndex:i] setHidden:YES];

        }
    }
    if ([rowField.text isEqual: @"2"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[aRow4 objectAtIndex:i] setHidden:YES];

            
        }
    }if ([rowField.text isEqual: @"3"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            [[aRow3 objectAtIndex:i] setHidden:NO];
            [[aRow4 objectAtIndex:i] setHidden:YES];

            
        }
    }
    if ([rowField.text isEqual: @"4"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            [[aRow3 objectAtIndex:i] setHidden:NO];
            [[aRow4 objectAtIndex:i] setHidden:NO];
            
        }
    }
    if ([acolumnField.text isEqual: @"1"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[acolumn2 objectAtIndex:i] setHidden:YES];
            [[acolumn3 objectAtIndex:i] setHidden:YES];
            [[acolumn4 objectAtIndex:i] setHidden:YES];
        }
    }
    if ([acolumnField.text isEqual: @"2"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[acolumn3 objectAtIndex:i] setHidden:YES];
            [[acolumn4 objectAtIndex:i] setHidden:YES];
            
        }
    }
    if ([acolumnField.text isEqual: @"3"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[acolumn4 objectAtIndex:i] setHidden:YES];
        }
    }
    /*if (acolumnField.text == @"4") {
        for (int i =0; i < [aRow1 count]; i++) {
            [[acolumn4 objectAtIndex:i] setHidden:NO];
        }
    }*/

    
    bRow1 = [[NSArray alloc] initWithObjects:b11,b12,b13,b14, nil];
    bRow2 = [[NSArray alloc] initWithObjects:b21,b22,b23,b24, nil];
    bRow3 = [[NSArray alloc] initWithObjects:b31,b32,b33,b34, nil];
    bRow4 = [[NSArray alloc] initWithObjects:b41,b42,b43,b44, nil];
    bcolumn1 = [[NSArray alloc] initWithObjects:b11,b21,b31, b41, nil];
    bcolumn2 = [[NSArray alloc] initWithObjects:b12,b22,b32, b42, nil];
    bcolumn3 = [[NSArray alloc] initWithObjects:b13,b23,b33, b43, nil];
    bcolumn4 = [[NSArray alloc] initWithObjects:b14,b24,b34, b44, nil];
    if ([bRowField.text isEqual: @"1"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bRow1 objectAtIndex:i] setHidden:NO];
            [[bRow2 objectAtIndex:i] setHidden:YES];
            [[bRow3 objectAtIndex:i] setHidden:YES];
            [[bRow4 objectAtIndex:i] setHidden:YES];
        }
    }
    if ([bRowField.text isEqual: @"2"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bRow1 objectAtIndex:i] setHidden:NO];
            [[bRow2 objectAtIndex:i] setHidden:NO];
            [[bRow3 objectAtIndex:i] setHidden:YES];
            [[bRow4 objectAtIndex:i] setHidden:YES];
            
        }
    }if ([bRowField.text isEqual: @"3"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bRow1 objectAtIndex:i] setHidden:NO];
            [[bRow2 objectAtIndex:i] setHidden:NO];
            [[bRow3 objectAtIndex:i] setHidden:NO];
            [[bRow4 objectAtIndex:i] setHidden:YES];
            
        }
    }
    if ([bRowField.text isEqual: @"4"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bRow1 objectAtIndex:i] setHidden:NO];
            [[bRow2 objectAtIndex:i] setHidden:NO];
            [[bRow3 objectAtIndex:i] setHidden:NO];
            [[bRow4 objectAtIndex:i] setHidden:NO];
            
        }
    }
    if ([bColumnField.text isEqual: @"1"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bcolumn2 objectAtIndex:i] setHidden:YES];
            [[bcolumn3 objectAtIndex:i] setHidden:YES];
            [[bcolumn4 objectAtIndex:i] setHidden:YES];
        }
    }
    if ([bColumnField.text isEqual: @"2"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bcolumn3 objectAtIndex:i] setHidden:YES];
            [[bcolumn4 objectAtIndex:i] setHidden:YES];
            
        }
    }if ([bColumnField.text isEqual: @"3"]) {
        for (int i =0; i < [bRow1 count]; i++) {
            [[bcolumn4 objectAtIndex:i] setHidden:YES];
        }
    }
}
-(IBAction)hidePicker:(id)sender{
    [sizePicker setHidden:YES];
    [operationPicker setHidden:YES];
    [bSizePicker setHidden:YES];
    [calcButton setEnabled:YES];
    [clearButton setEnabled:YES];
    [activeField resignFirstResponder];
    /*if (activeField) {
     [self textFieldDidBeginEditing:activeField];
     }*/
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    [sizePicker setHidden:YES];
    [operationPicker setHidden:YES];
    
    
    return NO;
}


-(IBAction) textFieldDoneEditing:(id)sender{
    [sizePicker setHidden:YES];
    [self textFieldShouldReturn:activeField];	
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    //if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar c = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only integer values."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        return NO;
    }
    
    
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    
    return YES;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
        self.navigationItem.title = @"2 Matrices";
        
    }
    return self;
}



-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        //[scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+120)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
    }
   }

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
}


-(void)showInfo{
    UIAlertView* instructions = [[UIAlertView alloc] initWithTitle:@"Help" message:@"To change matrix sizes, tap on the text fields under the corresponding matrix label\n Enter in the values in both Matrix A and B \n Choose your operation from the top text field and tap the calculate button." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [instructions show];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    showWork = [[UIBarButtonItem alloc] 
                initWithTitle:@"Show Work"                                            
                style:UIBarButtonItemStyleBordered 
                target:self 
                action:@selector(showWorkView)];

    infoButton = [[UIBarButtonItem alloc] initWithTitle:@"Help" style:UIBarButtonItemStyleBordered target:self action:@selector(showInfo)];
    self.navigationItem.rightBarButtonItem = infoButton;
    
    //self.navigationItem.hidesBackButton = YES;
    [self registerForKeyboardNotifications];
    
    sizePicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [sizePicker setDataSource: self];
    [sizePicker setDelegate: self];
    sizePicker.showsSelectionIndicator = YES;
    bSizePicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [bSizePicker setDataSource: self];
    [bSizePicker setDelegate: self];
    bSizePicker.showsSelectionIndicator = YES;
    operationPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [operationPicker setDataSource: self];
    [operationPicker setDelegate: self];
    operationPicker.showsSelectionIndicator = YES;
    
    operationField.inputView = operationPicker;
    rowField.inputView = sizePicker;
    acolumnField.inputView = sizePicker;
    bRowField.inputView = sizePicker;
    bColumnField.inputView = sizePicker;

    pickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    UIBarButtonItem* doneTyping = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTypingPressed)];
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //[doneTyping setTintColor:[UIColor blueColor]];
    /*[pickerToolbar setTintColor:[UIColor blackColor]];
     NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], UITextAttributeTextColor, [UIColor blackColor], UITextAttributeTextShadowColor, nil];
     [doneTyping setTitleTextAttributes:textTitleOptions forState:UIControlStateNormal];*/
    NSArray* items = [[NSArray alloc]initWithObjects:flexible, doneTyping, nil];
    [pickerToolbar setItems:items];
    
    operationField.inputAccessoryView = pickerToolbar;
    rowField.inputAccessoryView = pickerToolbar;
    acolumnField.inputAccessoryView = pickerToolbar;
    bRowField.inputAccessoryView = pickerToolbar;
    bColumnField.inputAccessoryView = pickerToolbar;
    
    sizesArray = [[NSArray alloc] initWithObjects:@"1",@"2",@"3",@"4", nil];
    [sizePicker selectRow:2 inComponent:0 animated:NO];  
    [sizePicker selectRow:2 inComponent:2 animated:NO];
    [bSizePicker selectRow:2 inComponent:0 animated:NO];  
    [bSizePicker selectRow:2 inComponent:2 animated:NO];
    rowField.text= [sizesArray objectAtIndex:[sizePicker selectedRowInComponent:0]];
    acolumnField.text= [sizesArray objectAtIndex:[sizePicker selectedRowInComponent:2]];
    bRowField.text = [sizesArray objectAtIndex:[bSizePicker selectedRowInComponent:0]];
    bColumnField.text = [sizesArray objectAtIndex:[bSizePicker selectedRowInComponent:2]];
    sizePicker.delegate = self;
    sizePicker.dataSource = self;
    bSizePicker.delegate = self;
    bSizePicker.dataSource = self;
    
    operationsArray = [[NSArray alloc] initWithObjects:@"Multiply", @"Add", @"Subtract", nil];
    operationField.text = [operationsArray objectAtIndex:[operationPicker selectedRowInComponent:0]]   ;
    [self configureMatrix];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]); 
    
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}

-(void)doneTypingPressed{
    
    [activeField resignFirstResponder];
    
    
}

#pragma Calculations
-(IBAction)clearButtonPressed:(id)sender{
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    self.navigationItem.rightBarButtonItems = nil;
    self.navigationItem.rightBarButtonItem = infoButton;
    NSArray* allElements = [[NSArray alloc] initWithObjects:a11,a12,a13, a14,a21,a22, a23, a24,a31,a32,a34,a33, a41,a42,a43,a44,b12,b11, b13,b14,b21,b22,b23,b24,b31,b32,b33,b34,b41,b42,b43,b44, nil];
    for (int i = 0; i < [allElements count]; i++) {
        
        UITextField* fieldArray =  [allElements objectAtIndex:i];
        fieldArray.text = @"0";
        
    }
    //r1c1.text = @"0";
    /* 
     x1TextField.text = @"";
     x2TextField.text = @"";
     y1TextField.text = @"";
     y2TextField.text = @"";*/
    answerTextView.text = @"";
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

-(IBAction)calculateButtonPressed:(id)sender;
{
        
    double numA11 = [a11.text doubleValue];
    double numA12 = [a12.text doubleValue];
    double numA13 = [a13.text doubleValue];
    double numA14 = [a14.text doubleValue];
    double numA21 = [a21.text doubleValue];
    double numA22 = [a22.text doubleValue];
    double numA23 = [a23.text doubleValue];
    double numA24 = [a24.text doubleValue];
    double numA31 = [a31.text doubleValue];
    double numA32 = [a32.text doubleValue];
    double numA33 = [a33.text doubleValue];
    double numA34 = [a34.text doubleValue];
    double numA41 = [a41.text doubleValue];
    double numA42 = [a42.text doubleValue];
    double numA43 = [a43.text doubleValue];
    double numA44 = [a44.text doubleValue];
    
    double numB11 = [b11.text doubleValue];
    double numB12 = [b12.text doubleValue];
    double numB13 = [b13.text doubleValue];
    double numB14 = [b14.text doubleValue];
    double numB21 = [b21.text doubleValue];
    double numB22 = [b22.text doubleValue];
    double numB23 = [b23.text doubleValue];
    double numB24 = [b24.text doubleValue];
    double numB31 = [b31.text doubleValue];
    double numB32 = [b32.text doubleValue];
    double numB33 = [b33.text doubleValue];
    double numB34 = [b34.text doubleValue];
    double numB41 = [b41.text doubleValue];
    double numB42 = [b42.text doubleValue];
    double numB43 = [b43.text doubleValue];
    double numB44 = [b44.text doubleValue];
    int aRows = [rowField.text intValue];
    int bColumns = [bColumnField.text intValue];
    int aColumns = [acolumnField.text intValue];
    int bRows = [bRowField.text intValue];
    NSMutableArray* blankArray = [[NSMutableArray alloc] initWithObjects:@" ",@" ",@" ",@" ", nil];
    NSMutableArray* aRow1Num = [[NSMutableArray alloc] init];
    [aRow1Num addObject:[NSNumber numberWithDouble:numA11]];
    [aRow1Num addObject:[NSNumber numberWithDouble:numA12]];
    [aRow1Num addObject:[NSNumber numberWithDouble:numA13]];
    [aRow1Num addObject:[NSNumber numberWithDouble:numA14]];
    NSMutableArray* aRow2Num =[[NSMutableArray alloc] init];
    [aRow2Num addObject:[NSNumber numberWithDouble:numA21]];
    [aRow2Num addObject:[NSNumber numberWithDouble:numA22]];
    [aRow2Num addObject:[NSNumber numberWithDouble:numA23]];
    [aRow2Num addObject:[NSNumber numberWithDouble:numA24]];
    NSMutableArray* aRow3Num =[[NSMutableArray alloc] init];
    [aRow3Num addObject:[NSNumber numberWithDouble:numA31]];
    [aRow3Num addObject:[NSNumber numberWithDouble:numA32]];
    [aRow3Num addObject:[NSNumber numberWithDouble:numA33]];
    [aRow3Num addObject:[NSNumber numberWithDouble:numA34]];
    NSMutableArray* aRow4Num =[[NSMutableArray alloc] init];
    [aRow4Num addObject:[NSNumber numberWithDouble:numA41]];
    [aRow4Num addObject:[NSNumber numberWithDouble:numA42]];
    [aRow4Num addObject:[NSNumber numberWithDouble:numA43]];
    [aRow4Num addObject:[NSNumber numberWithDouble:numA44]];
    aArray = [[NSMutableArray alloc] initWithObjects:aRow1Num, aRow2Num, aRow3Num, aRow4Num,  nil];
    
    for (int i = aArray.count -1; i > aRows -1; i--) {
        [aArray replaceObjectAtIndex:i withObject:blankArray];
    }    
    //remove columns in each rows
    for (int i = 0; i < aArray.count; i++) {
        NSMutableArray* arrayElement = [aArray objectAtIndex:i];
        for (int j = arrayElement.count - 1; j > aColumns - 1; j--) {
            [arrayElement removeLastObject];
        }
        
    }
    
    
    NSMutableArray* bRow1Num = [[NSMutableArray alloc] init];
    [bRow1Num addObject:[NSNumber numberWithDouble:numB11]];
    [bRow1Num addObject:[NSNumber numberWithDouble:numB12]];
    [bRow1Num addObject:[NSNumber numberWithDouble:numB13]];
    [bRow1Num addObject:[NSNumber numberWithDouble:numB14]];
    NSMutableArray* bRow2Num =[[NSMutableArray alloc] init];
    [bRow2Num addObject:[NSNumber numberWithDouble:numB21]];
    [bRow2Num addObject:[NSNumber numberWithDouble:numB22]];
    [bRow2Num addObject:[NSNumber numberWithDouble:numB23]];
    [bRow2Num addObject:[NSNumber numberWithDouble:numB24]];
    NSMutableArray* bRow3Num =[[NSMutableArray alloc] init];
    [bRow3Num addObject:[NSNumber numberWithDouble:numB31]];
    [bRow3Num addObject:[NSNumber numberWithDouble:numB32]];
    [bRow3Num addObject:[NSNumber numberWithDouble:numB33]];
    [bRow3Num addObject:[NSNumber numberWithDouble:numB34]];
    NSMutableArray* bRow4Num =[[NSMutableArray alloc] init];
    [bRow4Num addObject:[NSNumber numberWithDouble:numB41]];
    [bRow4Num addObject:[NSNumber numberWithDouble:numB42]];
    [bRow4Num addObject:[NSNumber numberWithDouble:numB43]];
    [bRow4Num addObject:[NSNumber numberWithDouble:numB44]];
    bArray = [[NSMutableArray alloc] initWithObjects:bRow1Num, bRow2Num, bRow3Num, bRow4Num,  nil];
    for (int i = bArray.count -1; i > bRows -1; i--) {
        [bArray replaceObjectAtIndex:i withObject:blankArray];
    }    
    //remove columns in each rows
    for (int i = 0; i < bArray.count; i++) {
        NSMutableArray* arrayElement = [bArray objectAtIndex:i];
        for (int j = arrayElement.count - 1; j > bColumns - 1; j--) {
            [arrayElement removeLastObject];
        }
        
    }
    
    if ([operationField.text isEqual: @"Multiply"]) {
        operationSymbol = @"X";
        
        if (aColumns == bRows) {
            
            double AB11 = numA11 * numB11 + numA12 * numB21 +numA13 * numB31 + numA14 * numB41;
            double AB12 = numA11 * numB12 + numA12 * numB22 +numA13 * numB32 + numA14 * numB42;
            double AB13 = numA11 * numB13 + numA12 * numB23 +numA13 * numB33 + numA14 * numB43;
            double AB14 = numA11 * numB14 + numA12 * numB24 +numA13 * numB34 + numA14 * numB44;
            
            double AB21 = numA21 * numB11 + numA22 * numB21 +numA23 * numB31 + numA24 * numB41;
            double AB22 = numA21 * numB12 + numA22 * numB22 +numA23 * numB32 + numA24 * numB42;
            double AB23 = numA21 * numB13 + numA22 * numB23 +numA23 * numB33 + numA24 * numB43;
            double AB24 = numA21 * numB14 + numA22 * numB24 +numA23 * numB34 + numA24 * numB44;
            
            double AB31 = numA31 * numB11 + numA32 * numB21 +numA33 * numB31 + numA34 * numB41;
            double AB32 = numA31 * numB12 + numA32 * numB22 +numA33 * numB32 + numA34 * numB42;
            double AB33 = numA31 * numB13 + numA32 * numB23 +numA33 * numB33 + numA34 * numB43;
            double AB34 = numA31 * numB14 + numA32 * numB24 +numA33 * numB34 + numA34 * numB44;
            
            double AB41 = numA41 * numB11 + numA42 * numB21 +numA43 * numB31 + numA44 * numB41;
            double AB42 = numA41 * numB12 + numA42 * numB22 +numA43 * numB32 + numA44 * numB42;
            double AB43 = numA41 * numB13 + numA42 * numB23 +numA43 * numB33 + numA44 * numB43;
            double AB44 = numA41 * numB14 + numA42 * numB24 +numA43 * numB34 + numA44 * numB44;
            NSMutableArray* answerRow1 = [[NSMutableArray alloc] init];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB11]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB12]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB13]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB14]];
            NSMutableArray* answerRow2 =[[NSMutableArray alloc] init];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB21]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB22]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB23]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB24]];
            NSMutableArray* answerRow3 =[[NSMutableArray alloc] init];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB31]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB32]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB33]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB34]];
            NSMutableArray* answerRow4 =[[NSMutableArray alloc] init];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB41]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB42]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB43]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB44]];
            //array with all answers
            answerArray = [[NSMutableArray alloc] initWithObjects:answerRow1, answerRow2, answerRow3, answerRow4, nil];
            
            //remove rows not needed
            for (int i = answerArray.count -1; i > aRows -1; i--) {
                [answerArray replaceObjectAtIndex:i withObject:blankArray];
            }    
            //remove columns in each rows
            for (int i = 0; i < answerArray.count; i++) {
                NSMutableArray* arrayElement = [answerArray objectAtIndex:i];
                for (int j = arrayElement.count - 1; j > bColumns - 1; j--) {
                    [arrayElement removeLastObject];
                }
                
            }
            NSString* arrayString = [NSString stringWithFormat:@"%@", answerArray];
            NSString* newString = [arrayString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
            newString = [newString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"," withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            answerTextView.text = [NSString stringWithFormat:@"%@",newString];
            [self createContentPages];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                NSArray* rightButtons = [[NSArray alloc] initWithObjects:infoButton, showWork, nil];
                [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
            }
            NSString* firstElement ;
            switch (aColumns) {
                case 1:
                    firstElement = [NSString stringWithFormat:@"AB11 = %.02f*%.02ff",numA11,numB11];
                    break;
                case 2:
                    firstElement = [NSString stringWithFormat:@"AB11 = %.02f*%.02f + %.02f*%.02f ",numA11,numB11,numA12,numB21];
                    break;
                case 3:
                    firstElement = [NSString stringWithFormat:@"AB11 = %.02f*%.02f + %.02f*%.02f + %.02f*%.02f ",numA11,numB11,numA12,numB21,numA13, numB31];
                    break;
                case 4:
                    firstElement = [NSString stringWithFormat:@"AB11 = %.02f*%.02f + %.02f*%.02f + %.02f*%.02f + %.02f*%.02f",numA11,numB11,numA12,numB21,numA13, numB31, numA44, numB44];
                    break;
                default:
                    break;
            }
            answerString = [NSString stringWithFormat: @"<html><head><style>td.blue {color:blue;}  td.red {color:red;}td.green {color:green;} td.orange {color:orange;} td.brown {color:brown;} td.cyan {color:cyan;}td.magenta {color:magenta;}</style></head><body><h2></h2><h3><table><tr><td>%@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td>%@</td></tr>     <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>     </table> </h3>    <p> Since the columns of matrix A are equal to the rows of matrix B, we can multiply the two matrices. To multiply the matrices we multiply the rows of A by the columns of B and add the products.<br /> For Example, the first element(AB11) of the answer for this problem would be:</p> <p>%@ = %.02f</p><p>Continuing in a similar fashion multiply each row of A by each column of B we get </p>                             <table><tr><td> %@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td >%@</td><td  rowspan='4'> &nbsp = &nbsp</td><td> %@</td></tr>     <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td> %@ </td><td > %@ </td></tr></table></h3>  </body></html>",  [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3], firstElement, AB11,                                                    [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[arrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1],[arrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[arrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3],[arrayOfStrings objectAtIndex:3] ];
            


        }else {
            UIAlertView* cantTimesAlert = [[UIAlertView alloc] initWithTitle:@"Matrix multiplication not possible!" message:@"To multiply matrices, the columns of matrix A must equal the rows of matrix B." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [cantTimesAlert show];
        }
       
        
        
    }else if([operationField.text isEqualToString:@"Add"]){
        operationSymbol = @"+";
        if (aRows == bRows && aColumns == bColumns){
            double AB11 = numA11 + numB11 ;
            double AB12 = numA12 + numB12 ;
            double AB13 = numA13 + numB13 ;
            double AB14 = numA14 + numB14 ;
            
            double AB21 = numA21 + numB21 ;
            double AB22 = numA22 + numB22 ;
            double AB23 = numA23 + numB23 ;
            double AB24 = numA24 + numB24 ;
            
            double AB31 = numA31 + numB31 ;
            double AB32 = numA32 + numB32 ;
            double AB33 = numA33 + numB33 ;
            double AB34 = numA34 + numB34 ;
            
            double AB41 = numA41 + numB41 ;
            double AB42 = numA42 + numB42 ;
            double AB43 = numA43 + numB43;
            double AB44 = numA44 + numB44 ;
            NSMutableArray* answerRow1 = [[NSMutableArray alloc] init];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB11]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB12]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB13]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB14]];
            NSMutableArray* answerRow2 =[[NSMutableArray alloc] init];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB21]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB22]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB23]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB24]];
            NSMutableArray* answerRow3 =[[NSMutableArray alloc] init];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB31]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB32]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB33]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB34]];
            NSMutableArray* answerRow4 =[[NSMutableArray alloc] init];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB41]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB42]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB43]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB44]];
            //array with all answers
            answerArray = [[NSMutableArray alloc] initWithObjects:answerRow1, answerRow2, answerRow3, answerRow4, nil];
            
            //remove rows not needed
            for (int i = answerArray.count -1; i > aRows -1; i--) {
                [answerArray replaceObjectAtIndex:i withObject:blankArray];
            }    
            //remove columns in each rows
            for (int i = 0; i < answerArray.count; i++) {
                NSMutableArray* arrayElement = [answerArray objectAtIndex:i];
                for (int j = arrayElement.count - 1; j > bColumns - 1; j--) {
                    [arrayElement removeLastObject];
                }
                
            }
            NSString* arrayString = [NSString stringWithFormat:@"%@", answerArray];
            NSString* newString = [arrayString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
            newString = [newString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"," withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            answerTextView.text = [NSString stringWithFormat:@"%@",newString];
            [self createContentPages];
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                NSArray* rightButtons = [[NSArray alloc] initWithObjects:infoButton, showWork, nil];
                [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
            }
            NSString* firstElement ;
            firstElement = [NSString stringWithFormat:@"AB11 = %.02f+%.02f",numA11,numB11];
            
            answerString = [NSString stringWithFormat: @"<html><head><style>td.blue {color:blue;}  td.red {color:red;}td.green {color:green;} td.orange {color:orange;} td.brown {color:brown;} td.cyan {color:cyan;}td.magenta {color:magenta;}</style></head><body><h2></h2><h3><table><tr><td>%@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td>%@</td></tr>     <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>     </table> </h3>    <p> Since the dimensions of matrix A are equal to the dimensions of matrix B, we can add the two matrices. To add the matrices we add the  elements of A to the corresponding elements of B.<br /> For Example, the first element(AB11) of the answer for this problem would be:</p> <p>%@ = %.02f</p><p>Continuing in a similar fashion add each element of A to each corresponding element of B we get </p>                             <table><tr><td> %@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td >%@</td><td  rowspan='4'> &nbsp = &nbsp</td><td> %@</td></tr>     <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td> %@ </td><td > %@ </td></tr></table></h3>  </body></html>",  [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3], firstElement, AB11,                                                    [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[arrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1],[arrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[arrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3],[arrayOfStrings objectAtIndex:3] ];
        }else {
            UIAlertView* cantAddAlert = [[UIAlertView alloc] initWithTitle:@"Matrix addition not possible!" message:@"To add matrices, the dimenstions of matrix A must equal the dimensions of matrix B." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [cantAddAlert show];
        }
        
    }else if([operationField.text isEqualToString:@"Subtract"]){
        operationSymbol = @"-";
        if (aRows == bRows && aColumns == bColumns){
            double AB11 = numA11 - numB11 ;
            double AB12 = numA12 - numB12 ;
            double AB13 = numA13 - numB13 ;
            double AB14 = numA14 - numB14 ;
            
            double AB21 = numA21 - numB21 ;
            double AB22 = numA22 - numB22 ;
            double AB23 = numA23 - numB23 ;
            double AB24 = numA24 - numB24 ;
            
            double AB31 = numA31 - numB31 ;
            double AB32 = numA32 - numB32 ;
            double AB33 = numA33 - numB33 ;
            double AB34 = numA34 - numB34 ;
            
            double AB41 = numA41 - numB41 ;
            double AB42 = numA42 - numB42 ;
            double AB43 = numA43 - numB43;
            double AB44 = numA44 - numB44 ;
            NSMutableArray* answerRow1 = [[NSMutableArray alloc] init];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB11]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB12]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB13]];
            [answerRow1 addObject:[NSNumber numberWithDouble:AB14]];
            NSMutableArray* answerRow2 =[[NSMutableArray alloc] init];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB21]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB22]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB23]];
            [answerRow2 addObject:[NSNumber numberWithDouble:AB24]];
            NSMutableArray* answerRow3 =[[NSMutableArray alloc] init];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB31]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB32]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB33]];
            [answerRow3 addObject:[NSNumber numberWithDouble:AB34]];
            NSMutableArray* answerRow4 =[[NSMutableArray alloc] init];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB41]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB42]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB43]];
            [answerRow4 addObject:[NSNumber numberWithDouble:AB44]];
            //array with all answers
            answerArray = [[NSMutableArray alloc] initWithObjects:answerRow1, answerRow2, answerRow3, answerRow4, nil];
            
            //remove rows not needed
            for (int i = answerArray.count -1; i > aRows -1; i--) {
                [answerArray replaceObjectAtIndex:i withObject:blankArray];
            }    
            //remove columns in each rows
            for (int i = 0; i < answerArray.count; i++) {
                NSMutableArray* arrayElement = [answerArray objectAtIndex:i];
                for (int j = arrayElement.count - 1; j > bColumns - 1; j--) {
                    [arrayElement removeLastObject];
                }
                
            }
            NSString* arrayString = [NSString stringWithFormat:@"%@", answerArray];
            NSString* newString = [arrayString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
            newString = [newString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"," withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            answerTextView.text = [NSString stringWithFormat:@"%@",newString];
            [self createContentPages];
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
                NSArray* rightButtons = [[NSArray alloc] initWithObjects:infoButton, showWork, nil];
                [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
            }
            NSString* firstElement ;
            firstElement = [NSString stringWithFormat:@"AB11 = %.02f-%.02f",numA11,numB11];
            
            answerString = [NSString stringWithFormat: @"<html><head><style>td.blue {color:blue;}  td.red {color:red;}td.green {color:green;} td.orange {color:orange;} td.brown {color:brown;} td.cyan {color:cyan;}td.magenta {color:magenta;}</style></head><body><h2></h2><h3><table><tr><td>%@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td>%@</td></tr>     <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>    <tr><td> %@ </td><td> %@ </td></tr>     </table> </h3>    <p> Since the dimensions of matrix A are equal to the dimensions of matrix B, we can subtract the two matrices. To subtract the matrices we subtract the  elements of B from the corresponding elements of A.<br /> For Example, the first element(AB11) of the answer for this problem would be:</p> <p>%@ = %.02f</p><p>Continuing in a similar fashion subtract each element of B from each corresponding element of A we get </p>                             <table><tr><td> %@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td >%@</td><td  rowspan='4'> &nbsp = &nbsp</td><td> %@</td></tr>     <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td > %@ </td><td > %@ </td></tr>    <tr><td > %@ </td><td> %@ </td><td > %@ </td></tr></table></h3>  </body></html>",  [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3], firstElement, AB11,                                                    [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[arrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1],[arrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[arrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3],[arrayOfStrings objectAtIndex:3] ];
            
        }else {
            UIAlertView* cantAddAlert = [[UIAlertView alloc] initWithTitle:@"Matrix subtraction not possible!" message:@"To subtract matrices, the dimenstions of matrix A must equal the dimensions of matrix B." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [cantAddAlert show];
        }
    }
        
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = answerString;
        [mainDetailViewController.answerWebView setHidden:YES];
        
        [mainDetailViewController configureView];
        
    }

    
    
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    //self.navigationBar = nil;
    
}

- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *contentString;
    arrayOfStrings = [[NSMutableArray alloc] init ];
    aArrayOfStrings = [[NSMutableArray alloc] init ];
    
    for (int i = 0; i < [answerArray count]; i++) {
        NSString* stringElement = [NSString stringWithFormat:@"%@", [answerArray objectAtIndex:i]];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"(" withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"," withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        
        [arrayOfStrings addObject:stringElement];
        
        
    }
    for (int i = 0; i < [aArray count]; i++) {
        NSString* stringElement = [NSString stringWithFormat:@"%@", [aArray objectAtIndex:i]];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"(" withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"," withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        
        [aArrayOfStrings addObject:stringElement];
        
        
    }
    bArrayOfStrings = [[NSMutableArray alloc] init ];

    NSLog(@"a array %@",aArrayOfStrings);
    
    
    for (int i = 0; i < [bArray count]; i++) {
        NSString* stringElement = [NSString stringWithFormat:@"%@", [bArray objectAtIndex:i]];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@")" withString:@"\n"];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"(" withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"," withString:@""];
        stringElement = [stringElement stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        
        [bArrayOfStrings addObject:stringElement];
        
        
    }
    NSLog(@"b array %@",bArrayOfStrings);
    //NSString* stringRow4 = [NSString stringWithFormat:@"%@", [answerArray objectAtIndex:3]];
    //NSString* newString = [stringRow4 stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    contentString = [NSString 
                     stringWithFormat:@"<html><head></head><body><h2>%@ <br /> <br /></h2><h3><table><tr><td>%@ </td><td  rowspan='4'> &nbsp %@ &nbsp</td><td>%@</td><td rowspan= '4'> &nbsp = &nbsp</td> <td>%@</td></tr>     <tr><td> %@ </td><td> %@ </td><td>%@</td></tr>    <tr><td> %@ </td><td> %@ </td><td>%@</td></tr>    <tr><td> %@ </td><td> %@ </td><td>%@</td></tr>     </table> </h3></body></html>",dateString, [aArrayOfStrings objectAtIndex:0], operationSymbol,[bArrayOfStrings objectAtIndex:0],[arrayOfStrings objectAtIndex:0],[aArrayOfStrings objectAtIndex:1],[bArrayOfStrings objectAtIndex:1],[arrayOfStrings objectAtIndex:1], [aArrayOfStrings objectAtIndex:2],[bArrayOfStrings objectAtIndex:2],[arrayOfStrings objectAtIndex:2],[aArrayOfStrings objectAtIndex:3],[bArrayOfStrings objectAtIndex:3],[arrayOfStrings objectAtIndex:3] ];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];
    }
}
-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}


@end

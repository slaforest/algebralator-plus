//
//  PythagoreanViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PythagoreanViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation PythagoreanViewController;

@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, navigationBar, scrollView, detailViewController, summaryViewController, mainDetailViewController, showWork;
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

#pragma - mark
#pragma iad Banner


-(void) viewWillAppear:(BOOL)animated{
    // [self moveBannerOffscreen  ];
    }

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    //if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only positive rational number values."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        return NO;
    }
    
    
    return YES;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    showWork = [[UIBarButtonItem alloc] 
                initWithTitle:@"Show Work"                                            
                style:UIBarButtonItemStyleBordered 
                target:self 
                action:@selector(showWorkView)];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
    self.aTextField.delegate = self;
    self.bTextField.delegate = self;
    self.cTextField.delegate = self;
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;
    
}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

- (IBAction) checkButtonPressed: (id) sender{
   
    a = [aTextField.text doubleValue];
    b = [bTextField.text doubleValue];
    c = [cTextField.text doubleValue];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self.navigationItem setRightBarButtonItem: showWork animated:YES];
    }
    //if (c > a && c > b) {
        
        if (a == 0){
            
            missingVariable = @"a";
            a = sqrt( c*c - b*b );
            answerTextView.text = [NSString stringWithFormat:@"%@ = %g", missingVariable, a];
        }
        else if (b == 0) {
            missingVariable = @"b";
            b = sqrt( c*c - a*a );
            answerTextView.text = [NSString stringWithFormat:@"%@ = %g", missingVariable, b];
        }
        else if (c == 0) {
            missingVariable = @"c";
            c = sqrt( a*a + b*b );
            answerTextView.text = [NSString stringWithFormat:@"%@ = %g", missingVariable, c];
        }
        else if (a != 0 && b != 0 && c != 0){
            UIAlertView *nothingToSolve = [[UIAlertView alloc] initWithTitle:@"Nothing to solve!"
                                                                     message:@"Make sure to leave the unknown variable blank. If you want to check whether a triangle is a right triangle use the Right Triangle Checker function."
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
            [nothingToSolve show];
            self.navigationItem.rightBarButtonItem = nil;

            
        }
        
    /*}else{
        UIAlertView *nothingToSolve = [[UIAlertView alloc] initWithTitle:@"Hypotenuse Must Be the Longest Side!"
                                                                 message:@"Make sure that your c value is the greatest value out of the three inputs."
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
        [nothingToSolve show];
        self.navigationItem.rightBarButtonItem = nil;

        
    }*/

        [self createContentPages];
    if ([missingVariable isEqualToString: @"a"]){
    answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style><p> Using the Pythagorean Theorem: </p><p> a<sup>2</sup> + b <sup>2</sup> = c<sup>2</sup> </p> <p>And subbing in our values </p><p> a<sup>2</sup> +  %g<sup>2</sup> = %g<sup>2</sup> </p><p> Which equals </p><p> a<sup>2</sup> + %g = %g </p> Getting a by itself: <p> a<sup>2</sup> + %g - <font color='blue'> %g </font> = %g - <font color='blue'> %g </font></p>  <p>a<sup>2</sup> = %g </p> <p>Solving for a to get: </p> <table><tr><td nowrap='nowrap'>&radic;<span style='text-decoration:overline'> a<span style='font-size: 10px;vertical-align:+50%%;'>2</span></span> = &radic;<span style='text-decoration:overline'> %g</td></tr></table><table><tr><td> a =<td class='answer'>%g</td></tr></table></html>", b, c, b*b,c*c,b*b,b*b,c*c, b*b,(c*c)-(b*b),(c*c)-(b*b),a];
    }
    else if([missingVariable isEqual: @"b"]){
        answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style><p> Using the Pythagorean Theorem: </p><p> a<sup>2</sup> + b <sup>2</sup> = c<sup>2</sup> </p> <p>And subbing in our values </p><p> %g<sup>2</sup> +  b<sup>2</sup> = %g<sup>2</sup> </p><p> Which equals </p><p> %g + b<sup>2</sup> = %g </p> Getting b by itself: <p> %g - <font color='blue'> %g </font> + b<sup>2</sup>  = %g - <font color='blue'> %g </font></p>  <p>b<sup>2</sup> = %g </p> <p>Solving for b to get: </p> <table><tr><td nowrap='nowrap'>&radic;<span style='text-decoration:overline'> b<span style='font-size: 10px;vertical-align:+50%%;'>2</span></span> = &radic;<span style='text-decoration:overline'> %g</td></tr></table><table><tr><td> b =<td class='answer'>%g</td></tr></table></html>", a, c,a*a,c*c,a*a,a*a,c*c, a*a,(c*c)-(a*a),(c*c)-(a*a),b];
    }else if([missingVariable isEqual: @"c"]){
         answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style><p> Using the Pythagorean Theorem: </p><p> a<sup>2</sup> + b <sup>2</sup> = c<sup>2</sup> </p> <p>And subbing in our values </p><p> %g<sup>2</sup> +  %g<sup>2</sup> = c<sup>2</sup> </p><p> Which equals </p><p> %g + %g = c<sup>2</sup> </p> <p>  Which equals:</p> <p> %g = c<sup>2</sup></p>  Solving for c to get: </p> <table><tr><td nowrap='nowrap'>&radic;<span style='text-decoration:overline'> %g</span> = &radic;<span style='text-decoration:overline'> c<span style='font-size: 10px;vertical-align:+50%%;'>2</span></span></td></tr></table><table><tr><td> c =<td class='answer'>%g</td></tr></table></html>", a, b,a*a, b*b,a*a + b*b,a*a + b*b,c];
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = answerString;
        [mainDetailViewController.answerWebView setHidden:YES];
        
        
        [mainDetailViewController configureView];
        
    }

}


- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];
    
    NSString *contentString; 
    if([missingVariable isEqual: @"a"]){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The triangle with leg of %g and hypotenuse of %g, has a second leg value of %@</h2></body></html>",dateString,b,c, answerTextView.text ];
    }else if([missingVariable isEqual: @"b"]){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The triangle with leg of %g and hypotenuse of %g, has a second leg value of %@</h2></body></html>",dateString,a,c, answerTextView.text];
    }else if([missingVariable isEqual: @"c"]){
        
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The triangle with legs of %g and %g has a hypotenuse of %@</h2></body></html>",dateString,a,b,answerTextView.text];
    
    }else {
        contentString = [NSString 
                         stringWithFormat:@""];
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];    }
}
-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}



@end

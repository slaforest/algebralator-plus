//
//  ShowWorkViewController.m
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ShowWorkViewController.h"

@implementation ShowWorkViewController
@synthesize textView, webView, html, scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.webView loadHTMLString:html baseURL:nil];
    self.navigationItem.title = @"Solution";
    
    [[self.webView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[self.webView layer] setBorderWidth:2.3];
    [[self.webView layer] setCornerRadius:15];
    [self.webView setClipsToBounds: YES];

    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void)viewDidDisappear:(BOOL)animated{
      self.html = @"";
    [self.webView loadHTMLString:html baseURL:nil];
  
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end

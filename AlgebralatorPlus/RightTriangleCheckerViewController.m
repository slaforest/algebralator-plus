//
//  RightTriangleCheckerViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RightTriangleCheckerViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation RightTriangleCheckerViewController;

@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, navigationBar, scrollView, showWork, detailViewController, summaryViewController, mainDetailViewController ;
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

-(void) viewWillAppear:(BOOL)animated{
    // [self moveBannerOffscreen  ];
    }

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}




# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    //if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only positive rational number values."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        return NO;
    }
    
    
    return YES;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    showWork = [[UIBarButtonItem alloc] 
                initWithTitle:@"Show Work"                                            
                style:UIBarButtonItemStyleBordered 
                target:self 
                action:@selector(showWorkView)];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
    self.aTextField.delegate = self;
    self.bTextField.delegate = self;
    self.cTextField.delegate = self;
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;
    
}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

- (IBAction) checkButtonPressed: (id) sender{
    
    a = [aTextField.text doubleValue];
    b = [bTextField.text doubleValue];
    c = [cTextField.text doubleValue];
   
    NSLog(@"a string = %g, b string = %g , c String = %g",a, b, c);
    if (c <= a || c <= b){
        UIAlertView *cIsNotBiggest = [[UIAlertView alloc] initWithTitle:@"Invalid Entry!"
                                                                message:@"The c value must be the longest side."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
        [cIsNotBiggest show];

    } else {
        if((a*a + b*b) == c*c){
            NSString *firstLine = [NSString stringWithFormat:@" Since  %g + %g = %g, the triangle is a right triangle.", a*a, b*b,c*c];
            answerTextView.text = firstLine;
        }else {
            NSString *firstLine = [NSString stringWithFormat:@"Since %g + %g ≠ %g, \n the triangle is NOT a right triangle.",a*a,b*b,c*c];
            answerTextView.text = firstLine;
        }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
    }
    
    
        if((a*a + b*b) == c*c){
            
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style> <p> Using the Pythagorean Theorem: </p><p> a<sup>2</sup> + b <sup>2</sup> = c<sup>2</sup> </p> <p> Subbing our values in we get: </p><p> %g<sup>2</sup> +  %g<sup>2</sup> = %g<sup>2</sup></p><p><p> Which equals:</p><p> %g + %g = %g</p> <p> Since %g = %g </p> <p> Our triangle is a right triangle. </p> </html>", a,b,c, a*a,b*b, c*c,(a*a +b*b), c*c ];
        } else{
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style> <p> Using the Pythagorean Theorem: </p><p> a<sup>2</sup> + b <sup>2</sup> = c<sup>2</sup> </p> <p> Subbing our values in we get: </p><p> %g<sup>2</sup> +  %g<sup>2</sup> = %g<sup>2</sup></p><p><p> Which equals:</p><p> %g + %g = %g</p> <p> Since %g &ne; %g </p> <p> Our triangle is not a right triangle. </p> </html>", a,b,c, a*a,b*b, c*c,(a*a +b*b), c*c ];
            
        }
        [self createContentPages];

}
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = answerString;
        [mainDetailViewController.answerWebView setHidden:YES];
        
        [mainDetailViewController configureView];
        
    }
    }



- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];
    
    NSString *contentString; 
    if((a*a + b*b) == c*c){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The triangle with legs of %g and %g and hypotenuse of %g, is a right triangle</h2></body></html>",dateString,a,b,c ];
    }else if((a*a + b*b) != c*c){
        contentString = [NSString 
                         stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The triangle with legs of %g and %g and hypotenuse of %g, is NOT a right triangle</h2></body></html>",dateString,a,b,c ];
    }else {
        contentString = [NSString 
                         stringWithFormat:@""];
    }

    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];    }
}
-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}



@end

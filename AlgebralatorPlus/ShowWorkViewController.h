//
//  ShowWorkViewController.h
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowWorkViewController : UIViewController
{
    
}
@property (nonatomic, retain) IBOutlet UITextView *textView;
@property (nonatomic, retain) IBOutlet UIWebView  *webView;
@property (nonatomic, retain) NSString *html;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

@end

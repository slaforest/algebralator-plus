//
//  DecimalsToFractionsViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"

@class DetailViewController;

@interface DecimalsToFractionsViewController : UIViewController <UITextFieldDelegate> 
{
    UITextField *activeField;
    int decimalValue;
    int wholeNumberValue;
    BOOL isRepeatingDecimal;
    int decimalLength;
    double rationalNumber;
    int  dotIndex;
    NSString *answerString;

}
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UILabel *overlineLabel;
@property (nonatomic, retain) IBOutlet UILabel *repeatDigitsLabel;
@property (nonatomic, retain) IBOutlet UIStepper *repeatDigitsStepper;
@property (nonatomic, retain) IBOutlet UITextField *decimalField;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;

- (void) createContentPages;

-(IBAction)stepperChanged:(id)sender;
-(IBAction)calculateButtonPressed:(id)sender;
-(IBAction)clearButtonPressed:(id)sender;
@end

//
//  SolveMatricesViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"



@class DetailViewController;

@interface SolveMatricesViewController : UIViewController <UITextFieldDelegate, UISplitViewControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource, UIWebViewDelegate>
{
    BOOL keyboardVisible;
    UITextField *activeField;
    UITextField *lastActiveField;
    NSString* solutionString;
     NSArray* sizesArray;
}
@property (weak, nonatomic) IBOutlet UITextView *answerTextView;
@property (weak, nonatomic) IBOutlet UILabel *rightBracket;
@property (weak, nonatomic) IBOutlet UILabel *leftBracket;
@property (weak, nonatomic) IBOutlet UILabel *zLabel;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;
@property (weak, nonatomic) IBOutlet UIWebView *answerWebView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;



@property (weak, nonatomic) IBOutlet UITextField *x1;
@property (weak, nonatomic) IBOutlet UITextField *x2;
@property (weak, nonatomic) IBOutlet UITextField *x3;
@property (weak, nonatomic) IBOutlet UITextField *y1;
@property (weak, nonatomic) IBOutlet UITextField *y2;
@property (weak, nonatomic) IBOutlet UITextField *y3;
@property (weak, nonatomic) IBOutlet UITextField *z1;
@property (weak, nonatomic) IBOutlet UITextField *z2;
@property (weak, nonatomic) IBOutlet UITextField *z3;
@property (weak, nonatomic) IBOutlet UITextField *b1;
@property (weak, nonatomic) IBOutlet UITextField *b2;
@property (weak, nonatomic) IBOutlet UITextField *b3;
@property (weak, nonatomic) IBOutlet UITextField *rowField;
@property(nonatomic, strong)  UITextField* activeField;

@property (nonatomic, retain) IBOutlet UIPickerView* sizePicker;

- (IBAction)clearButton:(id)sender;
- (IBAction)calculateButtonPressed:(id)sender;

@end

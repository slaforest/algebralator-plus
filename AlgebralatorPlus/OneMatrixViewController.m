//
//  OneMatrixViewController.m
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 3/22/13.
//
//

#import "OneMatrixViewController.h"
#import "ShowWorkViewController.h"
#import "DetailViewController.h"

@interface OneMatrixViewController ()

@end

@implementation OneMatrixViewController
@synthesize a11,a12,a13,a21,a22,a23,a31,a32,a33;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    _sizePicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [_sizePicker setDataSource: self];
    [_sizePicker setDelegate: self];
    _sizePicker.showsSelectionIndicator = YES;
   
    _operationPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 2000, self.view.frame.size.width, 200)];
    [_operationPicker   setDataSource: self];
    [_operationPicker setDelegate: self];
    _operationPicker.showsSelectionIndicator = YES;
    
    
    
    
    operationsArray = [[NSArray alloc] initWithObjects:@"Determinant",@"Inverse", nil];
    sizesArray = [[NSArray alloc] initWithObjects: @"3 X 3", @"2 X 2",@"1 X 1", nil];

    _operationField.inputView = _operationPicker;
    _rowField.inputView = _sizePicker;
    _acolumnField.inputView = _sizePicker;
   
    pickerToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    UIBarButtonItem* doneTyping = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTypingPressed)];
    UIBarButtonItem *flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    NSArray* items = [[NSArray alloc]initWithObjects:flexible, doneTyping, nil];
    [pickerToolbar setItems:items];

    _operationField.inputAccessoryView = pickerToolbar;
    _rowField.inputAccessoryView = pickerToolbar;
    _acolumnField.inputAccessoryView = pickerToolbar;

    _showWork = [[UIBarButtonItem alloc]
                initWithTitle:@"Show Work"
                style:UIBarButtonItemStyleBordered
                target:self
                action:@selector(showWorkView)];
    
    infoButton = [[UIBarButtonItem alloc] initWithTitle:@"Help" style:UIBarButtonItemStyleBordered target:self action:@selector(showInfo)];
    self.navigationItem.rightBarButtonItem = infoButton;
    
    [self registerForKeyboardNotifications];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [_scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        _scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];

    [[_answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[_answerTextView layer] setBorderWidth:2.3];
    [[_answerTextView layer] setCornerRadius:15];
    [_answerTextView setClipsToBounds: YES];

    [[_answerWebView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[_answerWebView layer] setBorderWidth:2.3];
    [[_answerWebView layer] setCornerRadius:15];
    [_answerWebView setClipsToBounds: YES];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    if (pickerView == _operationPicker){
        return operationsArray.count;
    }else{
       
        return sizesArray.count;
        
    }

}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
        if (pickerView == _operationPicker){
            _operationField.text = [operationsArray objectAtIndex:row];
        }else{
            
                _rowField.text = [sizesArray objectAtIndex:row];
            
        }
    [self configureMatrix];

}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    

    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _activeField = textField;
            
}


-(void)doneTypingPressed{
    
    [_activeField resignFirstResponder];
    
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    if (pickerView == _operationPicker){
        return [operationsArray objectAtIndex:row];
    }else{
        if (component == 0 || component == 2) {
            return [sizesArray objectAtIndex:row];
        }else{
            return @"X";
        }
    }
    
}

-(void)movePickerViewOffScreen:(UIPickerView*) pickerView{
    
    CGRect frame = pickerView.frame;
    pickerView.frame = CGRectMake(frame.origin.x, 2000,frame.size.width, frame.size.height);
    // Step 2: Adjust the bottom content inset of your scroll view by the keyboard height.
    
}
-(void)bringPickerViewOnScreen:(UIPickerView*) pickerView{
    
    CGRect frame = pickerView.frame;
    pickerView.frame = CGRectMake(frame.origin.x, self.view.frame.size.height,frame.size.width, frame.size.height);
    

    
}

-(void)configureMatrix{
    
    aRow1 = [[NSArray alloc] initWithObjects:a11,a12,a13, nil];
    aRow2 = [[NSArray alloc] initWithObjects:a21,a22,a23, nil];
    aRow3 = [[NSArray alloc] initWithObjects:a31,a32,a33, nil];
    acolumn1 = [[NSArray alloc] initWithObjects:a11,a21,a31, nil];
    acolumn2 = [[NSArray alloc] initWithObjects:a12,a22,a32, nil];
    acolumn3 = [[NSArray alloc] initWithObjects:a13,a23,a33, nil];
    if ([_rowField.text isEqual: @"1 X 1"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            
            
            
        } for (int i =0; i < [aRow1 count]; i++) {
            [[aRow2 objectAtIndex:i] setHidden:YES];
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[acolumn2 objectAtIndex:i] setHidden:YES];
            [[acolumn3 objectAtIndex:i] setHidden:YES];
        }
    }
    if ([_rowField.text isEqual: @"2 X 2"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            
        }
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow3 objectAtIndex:i] setHidden:YES];
            [[acolumn3 objectAtIndex:i] setHidden:YES];

        }
    }if ([_rowField.text isEqual: @"3 X 3"]) {
        for (int i =0; i < [aRow1 count]; i++) {
            [[aRow1 objectAtIndex:i] setHidden:NO];
            [[aRow2 objectAtIndex:i] setHidden:NO];
            [[aRow3 objectAtIndex:i] setHidden:NO];
            
            
        }
    }
    
       
    }

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    return YES;
}
-(IBAction)calculateButtonPressed:(id)sender{
    double numA11 = [a11.text doubleValue];
    double numA12 = [a12.text doubleValue];
    double numA13 = [a13.text doubleValue];
    double numA21 = [a21.text doubleValue];
    double numA22 = [a22.text doubleValue];
    double numA23 = [a23.text doubleValue];
    double numA31 = [a31.text doubleValue];
    double numA32 = [a32.text doubleValue];
    double numA33 = [a33.text doubleValue];
    //NSString* solutionString;
    NSString* answerTextString;
    double det = 0.0;
    if ([_operationField.text isEqual:@"Determinant"]) {
        
        
        if ([_rowField.text isEqual:@"1 X 1"]) {
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn></mtd> </mtr> </mtable> <mo> | </mo> </mrow> <mo> = </mo> <mn>%g</mn> </math> </body></html>", numA11, numA11];
            
            solutionString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; '><body ><math mathsize='1.5em'><mtext> The determinant of a 1 X 1 matrix is the value of it's single element. Therefore,</mtext> <mrow>  <br /> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn></mtd> </mtr> </mtable> <mo> | </mo> </mrow> <mo> = </mo> <mn>%g</mn> </math> </body></html>", numA11, numA11];
            det = numA11;
        }else if ([_rowField.text isEqual:@"2 X 2"]) {
           det = [self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA21 row2col2:numA22];
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr> <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr></mtable> <mo> | </mo> </mrow> <mo> = </mo> <mn> %g</mn> </math> </body></html>", numA11,numA12,numA21,numA22, det];
            
            solutionString = [NSString stringWithFormat: @"<html><style> html {text-align:center; vertical-align:center;} math mtext{mathsize:1.2em; text-align:left} .answer{mathcolor:red;}</style>      <math  style='text-align:left'>    <mtext> The determinant of a 2 X 2 matrix is calculated in the following way:  </mtext></math> <br> <math> <mrow ><mo> | </mo> <mtable> <mtr> <mtd><mn> a </mn> </mtd> &nbsp; <mtd><mn> b </mn></mtd> </mtr> <mtr> <mtd><mn> c </mn></mtd> &nbsp; <mtd><mn> d </mn></mtd> </mtr></mtable> <mo> | </mo> </mrow> <mo> = </mo> <mn> a</mn> <mn>d </mn> <mo> - </mo><mn> b</mn><mn>c</mn></math>    <br/><br/>    <math class='text'> <mtext> In this case we have: </mtext</math> <br/><br/> <math><math> <mrow ><mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr> <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr></mtable> <mo> | </mo> </mrow> <mo> = </mo> <mn> %g</mn><mo> &times;</mo> <mn>%g </mn> <mo> - </mo><mn> %g</mn><mo> &times</mo><mn>%g</mn></math>   <br/><br/> <math><mrow> <mo> = <mo> <mn class='answer'> %g </mm></mrow></math>  </html>",numA11,numA12,numA21,numA22,numA11,numA22,numA12,numA21, det];
        }else if ([_rowField.text isEqual:@"3 X 3"]) {
           double det11 = [self get2By2Determinant:numA22 row1col2:numA23 row2col1:numA32 row2col2:numA33];
            double det12 = [self get2By2Determinant:numA21 row1col2:numA23 row2col1:numA31 row2col2:numA33];
            double det13 = [self get2By2Determinant:numA21 row1col2:numA22 row2col1:numA31 row2col2:numA32];
            //double detString = numA11*det11 - numA12*det12 + numA13*det13;
            
            
            
            det = [self get3By3Determinant:numA11 row1col2:numA12 row1col3:numA13 row2col1:numA21 row2col2:numA22 row2col3:numA23 row3col1:numA31 row3col2:numA32 row3col3:numA33];
            //NSLog(@"%g, %g, %g, %g,", det11,det12,det13,det);
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>  </mtable> <mo> | </mo> </mrow> <mo> = </mo>             <mn> %g</mn> </math> </body></html>", numA11,numA12,numA13,numA21,numA22,numA23,numA31, numA32, numA33, det];
            
            solutionString = [NSString stringWithFormat: @"<html><style> html {text-align:center; vertical-align:center;} math mtext{mathsize:1.2em; text-align:left} .answer{mathcolor:red;}</style>      <math  style='text-align:left'>  <math>  <mtext> The determinant of a 3 X 3 matrix is calculated in the following way:  </mtext><mtext> We first get the cofactor for each element in the first row.</mtext></math>  <br/><br/> <math><mrow> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>  </mtable> <mo> | </mo> </mrow> <mo> = </mo>                         <mrow> <mn> %g</mn><mo> &times; </mo><mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr> <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr></mtable> <mo> | </mo>  <mo> - </mo>    <mn> %g</mn><mo> &times; </mo><mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr> <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr></mtable> <mo> | </mo> <mo> + </mo>    <mn> %g</mn><mo> &times; </mo><mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr> <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr></mtable> <mo> | </mo></mrow></math>   <br><br> <math><mrow><mo> = </mo><mo> ( </mo> <mn>%g </mn> <mo> &times; </mo> <mn> %g </mn><mo> ) </mo> <mo> - </mo> <mo> ( </mo><mn>%g </mn> <mo> &times; </mo> <mn> %g </mn><mo> ) </mo> <mo> + </mo> <mo> ( </mo><mn>%g </mn> <mo> &times; </mo> <mn> %g </mn> <mo> ) </mo></mrow></math>          <br><br> <mo> = </mo> <mn> %g</mn> </math> </body></html>", numA11,numA12,numA13,numA21,numA22,numA23,numA31, numA32, numA33,numA11, numA22, numA23, numA32, numA33,numA12, numA21, numA23, numA31, numA33,numA13, numA21,numA22, numA31, numA32,numA11,det11,numA12, det12,numA13, det13, det];
            
            
            //solutionString = [NSString stringWithFormat: @"<html><style> html {text-align:center; vertical-align:center;} math mtext{mathsize:1.2em; text-align:left} .answer{mathcolor:red;}</style>      <math  style='text-align:left'>  <math>  <mtext> The determinant of a 3 X 3 matrix is calculated in the following way:  </mtext><mtext> First rewrite the first two columns to the right of the matrix</mtext></math>  <br/><br/> <math><mrow> <mo> | </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>  </mtable> <mo> | </mo><mtable><mtr><mtd> %g </mtd>&nbsp;<mtd> %g </mtd></mtr><mtr><mtd> %g </mtd>&nbsp;<mtd> %g </mtd></mtr><mtr><mtd> %g </mtd>&nbsp;<mtd> %g </mtd></mtr>       </mrow> <mo> = </mo> <mn> %g</mn> </math> </body></html>", numA11,numA12,numA13,numA21,numA22,numA23,numA31, numA32, numA33,numA11,numA12,numA21,numA22,numA31,numA32, det];
        }
        answerTextString = [NSString stringWithFormat:@"The determinant is %g", det];
    }else{
        if ([_rowField.text isEqual:@"1 X 1"]) {
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mtext> If</mtext> <mi> A </mi> <mo> = </mo>  <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd></mtr></mtable><mo> ) </mo> &nbsp; <mtext> then, </mtext>     <msup> <mi> A</mi> <mn> -1</mn></msup> <mo> = </mo> <mfrac><mn> 1 </mn><mn> %g </mn></mfrac>", numA11, numA11];
            
            solutionString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; '><body ><math mathsize='1.5em'><mtext> The inverse of a 1 X 1 matrix is the inverse of it's single element. Therefore,</mtext></math> <br /> <math> <mrow>   <mo> [ </mo> <mtable> <mtr> <mtd><mn> %g </mn></mtd> </mtr> </mtable> <mo> ] </mo> </mrow> <mo> = </mo> <mfrac><mn> 1 </mn><mn> %g </mn></mfrac> </math> </body></html>", numA11, numA11];
            
            det = numA11;
        }else if ([_rowField.text isEqual:@"2 X 2"]) {
            det = [self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA21 row2col2:numA22];
            double inv11 = numA22/det;
            double inv12 = -1*numA12/det;
            double inv21 = -1*numA21/det;
            double inv22 = numA11/det;
            
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mtext> If</mtext> <mi> A </mi> <mo> = </mo>  <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>        </mtable> <mo> ) </mo> <mtext> then, </mtext> </mrow>         <mrow> <msup> <mi> A</mi> <mn> -1</mn></msup> <mo> = </mo>   <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>     </mtable> <mo> ) </mo> </mrow> </math> </body></html>", numA11,numA12,numA21,numA22, inv11 ,inv12,inv21,inv22];
            
            solutionString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; '><body ><math mathsize='1.5em'><mrow> <mtext> If</mtext> <mi> A </mi> <mo> = </mo>  <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>        </mtable> <mo> ) </mo> <mtext> then, </mtext> </mrow> </math> <br/><math mathsize='1.5em'>        <mrow> <msup> <mi> A</mi> <mn> -1</mn></msup> <mo> = </mo>  <mfrac><mn> 1 </mn><mi>det A</mi></mfrac> <msup> <mi> C </mi> <mi> T </mi></msup>  </math> <br/><math><mi> where </mi> <msup><mi> C</mi><mi>T </mi></msup> <mo> = </mo><mi>the transpose of the cofactor matrix also known as the adjugate matrix</mi></math><br/><br/>     <math mathsize='1.5em'> <mo> = </mo> <mfrac><mn> 1 </mn> <mn> %g </mn></mfrac>         <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>     </mtable> <mo> ) </mo> </mrow> </math>    <br/><br/>       <math mathsize='1.5em'> <mo> = </mo><mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>     </mtable> <mo> ) </mo> </mrow> </math> </body></html>", numA11,numA12,numA21,numA22,det,numA22, -1*numA12, -1*numA21, numA11, inv11 ,inv12,inv21,inv22];

            
            
            
        }else if ([_rowField.text isEqual:@"3 X 3"]) {
        //getting cofactor matrix
        double det11 = [self get2By2Determinant:numA22 row1col2:numA23 row2col1:numA32 row2col2:numA33];
        double det12 = -1*[self get2By2Determinant:numA21 row1col2:numA23 row2col1:numA31 row2col2:numA33];
        double det13 = [self get2By2Determinant:numA21 row1col2:numA22 row2col1:numA31 row2col2:numA32];
        
        double det21 = -1*[self get2By2Determinant:numA12 row1col2:numA13 row2col1:numA32 row2col2:numA33];
        double det22 = [self get2By2Determinant:numA11 row1col2:numA13 row2col1:numA31 row2col2:numA33];
        double det23 = -1*[self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA31 row2col2:numA32];
        
        double det31 = [self get2By2Determinant:numA12 row1col2:numA13 row2col1:numA22 row2col2:numA23];
        double det32 = -1*[self get2By2Determinant:numA11 row1col2:numA13 row2col1:numA21 row2col2:numA23];
        double det33 = [self get2By2Determinant:numA11 row1col2:numA12 row2col1:numA21 row2col2:numA22];
        //NSArray* cofactorMatrix = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:det11], [NSNumber numberWithDouble:det12],[NSNumber numberWithDouble:det13],[NSNumber numberWithDouble:det21],[NSNumber numberWithDouble:det22],[NSNumber numberWithDouble:det23],[NSNumber numberWithDouble:det31],[NSNumber numberWithDouble:det32],[NSNumber numberWithDouble:det33], nil];
        
        //get det of 3x3
        det = [self get3By3Determinant:numA11 row1col2:numA12 row1col3:numA13 row2col1:numA21 row2col2:numA22 row2col3:numA23 row3col1:numA31 row3col2:numA32 row3col3:numA33];
        
        //getting adjugate (i.e.transpose of cofactor)
        double adj11 = det11;
        double adj12 = det21;
        double adj13 = det31;
        double adj21 = det12;
        double adj22 = det22;
        double adj23 = det32;
        double adj31 = det13;
        double adj32 = det23;
        double adj33 = det33;
        
       // NSArray* adjugateMatrix = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:adj11], [NSNumber numberWithDouble:adj12],[NSNumber numberWithDouble:adj13],[NSNumber numberWithDouble:adj21],[NSNumber numberWithDouble:adj22],[NSNumber numberWithDouble:adj23],[NSNumber numberWithDouble:adj31],[NSNumber numberWithDouble:adj32],[NSNumber numberWithDouble:adj33], nil];
        
        //get inverse
        double inv11 = adj11/det;
        double inv12 = adj12/det;
        double inv13 = adj13/det;
        double inv21 = adj21/det;
        double inv22 = adj22/det;
        double inv23 = adj23/det;
        double inv31 = adj31/det;
        double inv32 = adj32/det;
        double inv33 = adj33/det;
        
        htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'><body ><math mathsize='1.5em'><mrow> <mtext> If</mtext> <mi> A </mi> <mo> = </mo>  <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>  </mtable> <mo> ) </mo> <mtext> then, </mtext> </mrow>         <mrow> <msup> <mi> A</mi> <mn> -1</mn></msup> <mo> = </mo>   <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;<mtd><mn> %g </mn></mtd> </mtr>  </mtable> <mo> ) </mo> </mrow> </math> </body></html>", numA11,numA12,numA13,numA21,numA22,numA23,numA31, numA32, numA33, inv11   ,inv12,inv13,inv21,inv22,inv23,inv31, inv32, inv33];

            solutionString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; '><body ><math mathsize='1.2em'><mrow> <mtext> If</mtext> <mi> A </mi> <mo> = </mo>  <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp; <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>        </mtable> <mo> ) </mo> <mtext> then, </mtext> </mrow> </math> <br/><math mathsize='1.2em'>        <mrow> <msup> <mi> A</mi> <mn> -1</mn></msup> <mo> = </mo>  <mfrac><mn> 1 </mn><mi>det A</mi></mfrac> <msup> <mi> C </mi> <mi> T </mi></msup>  </math> <br/>   <math><mi> where </mi> <msup><mi> C</mi><mi>T </mi></msup> <mo> = </mo><mi>the transpose of the cofactor matrix also known as the adjugate matrix</mi></math><br/><br/>     <math mathsize='1.5em'> <mo> = </mo> <mfrac><mn> 1 </mn> <mn> %g </mn></mfrac>         <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>    <mtr> <mtd><mn> %g </mn></mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>      </mtable> <mo> ) </mo> </mrow> </math>       <br/><br/>       <math mathsize='1.2em'> <mo> ( </mo> <mtable> <mtr> <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>       <mtr> <mtd><mn> %g </mn></mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>    <mtr> <mtd><mn> %g </mn></mtd> &nbsp;  <mtd><mn> %g </mn> </mtd> &nbsp;  <mtd><mn> %g </mn></mtd> </mtr>      </mtable> <mo> ) </mo> </mrow> </math>  </body></html>", numA11,numA12, numA13, numA21,numA22,numA23,numA31, numA32, numA33, det,adj11, adj12, adj13, adj21, adj22,adj23,adj31, adj32, adj33, inv11 ,inv12,inv13,inv21,inv22, inv23, inv31, inv32, inv33];

        
        
        }
        if(det == 0){
            htmlString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; color:#00FF00'> <h2>The determinant is zero and this matrix is singular, therefore, this matrix does not have an inverse.</h2></html>"];
            solutionString = [NSString stringWithFormat: @"<html style='text-align:center; vertical-align:center; '> <h2>The determinant is zero and this matrix is singular, therefore, this matrix does not have an inverse.</h2></html>"];
            
        }

        
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        NSArray* rightButtons = [[NSArray alloc] initWithObjects:infoButton, _showWork, nil];
        [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
    }
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        //_mainDetailViewController.answerString = answerTextView.text;
        _mainDetailViewController.solutionString = solutionString;
        [_mainDetailViewController.answerWebView setHidden:NO];
        [_mainDetailViewController.answerWebView loadHTMLString:htmlString baseURL:nil];
        
        [_mainDetailViewController configureView];
        
    }
    [self createContentPages];
    [_answerWebView loadHTMLString:htmlString baseURL:nil];

}

-(double)get2By2Determinant:(double)r1c1 row1col2:(double)r1c2  row2col1: (double)r2c1 row2col2: (double)r2c2{
    double det = 0;
    det = r1c1 * r2c2 - r1c2 * r2c1;
    return det;
}

-(double)get3By3Determinant:(double)r1c1 row1col2:(double)r1c2 row1col3:(double)r1c3 row2col1: (double)r2c1 row2col2: (double)r2c2 row2col3:(double)r2c3 row3col1:(double)r3c1 row3col2:(double)r3c2 row3col3:(double)r3c3{
    double det11 = [self get2By2Determinant:r2c2 row1col2:r2c3 row2col1:r3c2 row2col2:r3c3];
    double det12 = [self get2By2Determinant:r2c1 row1col2:r2c3 row2col1:r3c1 row2col2:r3c3];
    double det13 = [self get2By2Determinant:r2c1 row1col2:r2c2 row2col1:r3c1 row2col2:r3c2];
    double det = r1c1*det11 - r1c2*det12 + r1c3*det13;
    return det;

}
- (void) createContentPages{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSString *contentString;
    
    
    contentString = [NSString
                     stringWithFormat:@"<html><head></head><body style='color:black;'><h2>%@ <br /> <br />%@</body></html>",dateString,htmlString];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [_mainDetailViewController viewWillAppear:YES];
    }

}


-(IBAction)clearButtonPressed:(id)sender{
    _mainDetailViewController.answerString = @"";
    _mainDetailViewController.solutionString = @"";
    [_mainDetailViewController.answerWebView loadHTMLString:@"" baseURL:nil];
    [_mainDetailViewController configureView];
    NSArray* allElements = [[NSArray alloc] initWithObjects:a11,a12,a13,a21,a22, a23,a31,a32,a33, nil];
    for (int i = 0; i < [allElements count]; i++) {
        
        UITextField* fieldArray =  [allElements objectAtIndex:i];
        fieldArray.text = @"0";
        [_answerWebView loadHTMLString:@"" baseURL:nil];
        self.navigationItem.rightBarButtonItems = nil;
        [self.navigationItem setRightBarButtonItem:infoButton];
        
    }
}

-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = solutionString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:solutionString baseURL:nil];
    
}

-(void)showInfo{
    UIAlertView* instructions = [[UIAlertView alloc] initWithTitle:@"Help" message:@"To change matrix dimensions, tap on the text field labeled 'Matrix Dimensions'\n Enter in the values in Matrix A  \n Choose your operation from the top text field and tap the calculate button." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [instructions show];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    //activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        UIInterfaceOrientation orientation = self.interfaceOrientation;
        if (UIDeviceOrientationIsPortrait(orientation)) {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,50);//- activeField.frame.origin.y) ;//-kbSize.height);
                [_scrollView setContentOffset:scrollPoint animated:YES];
            }
            
        }else {
            CGRect aRect = self.view.frame;
            aRect.size.height -= kbSize.height;
            if (!CGRectContainsPoint(aRect, _activeField.frame.origin) ) {
                CGPoint scrollPoint = CGPointMake(0.0,135);//- activeField.frame.origin.y) ;//-kbSize.height);
                [_scrollView setContentOffset:scrollPoint animated:YES];
            }
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    _scrollView.contentInset = contentInsets;
    _scrollView.scrollIndicatorInsets = contentInsets;
    [_activeField resignFirstResponder];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

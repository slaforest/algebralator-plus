//
//  TwoMatrixViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 9/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"
#import <iAd/iAd.h>

#define kKeyboardAnimationDuration .3
#define kTabBarHeight 50
@interface TwoMatrixViewController : UIViewController <UITextFieldDelegate, ADBannerViewDelegate, UISplitViewControllerDelegate, UIPickerViewDelegate,UIPickerViewDataSource> 

{
    
    BOOL keyboardVisible;
    UITextField *activeField;
    UITextField *lastActiveField;
    NSArray* sizesArray;
    NSArray* operationsArray;
    NSArray* aRow1 ;
    NSArray* aRow2 ;
    NSArray* aRow3 ;
    NSArray* aRow4 ;
    NSArray* acolumn1;
    NSArray* acolumn2 ;
    NSArray* acolumn3 ;
    NSArray* acolumn4;
    NSArray* bRow1 ;
    NSArray* bRow2 ;
    NSArray* bRow3 ;
    NSArray* bRow4 ;
    NSArray* bcolumn1;
    NSArray* bcolumn2 ;
    NSArray* bcolumn3 ;
    NSArray* bcolumn4;
    NSMutableArray* answerArray;
    NSMutableArray* aArray;
    NSMutableArray* bArray;
    NSString* operationSymbol;
    UIBarButtonItem* infoButton;
    NSString* answerString;
    NSMutableArray* arrayOfStrings ;
    NSMutableArray* aArrayOfStrings ;
    NSMutableArray* bArrayOfStrings ;
    UIToolbar* pickerToolbar;


    
}
@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;


@property (nonatomic, retain) IBOutlet UITextField* rowField;
@property (nonatomic, retain) IBOutlet UITextField* acolumnField;
@property (nonatomic, retain) IBOutlet UITextField* bRowField;
@property (nonatomic, retain) IBOutlet UITextField* bColumnField;
@property (nonatomic, retain) IBOutlet UITextField* operationField;
@property (nonatomic, retain) IBOutlet UIPickerView* sizePicker;
@property (nonatomic, retain) IBOutlet UIPickerView* operationPicker;
@property (nonatomic, retain) IBOutlet UIPickerView* bSizePicker;


@property (nonatomic, retain) IBOutlet UITextField* a11;
@property (nonatomic, retain) IBOutlet UITextField* a12;
@property (nonatomic, retain) IBOutlet UITextField* a13;
@property (nonatomic, retain) IBOutlet UITextField* a14;
@property (nonatomic, retain) IBOutlet UITextField* a21;
@property (nonatomic, retain) IBOutlet UITextField* a22;
@property (nonatomic, retain) IBOutlet UITextField* a23;
@property (nonatomic, retain) IBOutlet UITextField* a24;
@property (nonatomic, retain) IBOutlet UITextField* a31;
@property (nonatomic, retain) IBOutlet UITextField* a32;
@property (nonatomic, retain) IBOutlet UITextField* a33;
@property (nonatomic, retain) IBOutlet UITextField* a34;
@property (nonatomic, retain) IBOutlet UITextField* a41;
@property (nonatomic, retain) IBOutlet UITextField* a42;
@property (nonatomic, retain) IBOutlet UITextField* a43;
@property (nonatomic, retain) IBOutlet UITextField* a44;
@property (nonatomic, retain) IBOutlet UITextField* b11;
@property (nonatomic, retain) IBOutlet UITextField* b12;
@property (nonatomic, retain) IBOutlet UITextField* b13;
@property (nonatomic, retain) IBOutlet UITextField* b14;
@property (nonatomic, retain) IBOutlet UITextField* b21;
@property (nonatomic, retain) IBOutlet UITextField* b22;
@property (nonatomic, retain) IBOutlet UITextField* b23;
@property (nonatomic, retain) IBOutlet UITextField* b24;
@property (nonatomic, retain) IBOutlet UITextField* b31;
@property (nonatomic, retain) IBOutlet UITextField* b32;
@property (nonatomic, retain) IBOutlet UITextField* b33;
@property (nonatomic, retain) IBOutlet UITextField* b34;
@property (nonatomic, retain) IBOutlet UITextField* b41;
@property (nonatomic, retain) IBOutlet UITextField* b42;
@property (nonatomic, retain) IBOutlet UITextField* b43;
@property (nonatomic, retain) IBOutlet UITextField* b44;
@property (nonatomic, retain) IBOutlet UIButton* calcButton;
@property (nonatomic, retain) IBOutlet UIButton* clearButton;




-(IBAction)calculateButtonPressed:(id)sender;
- (void) createContentPages;
-(IBAction)clearButtonPressed:(id)sender;
-(IBAction)hidePicker:(id)sender;
@end

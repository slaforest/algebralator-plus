//
//  VertexFindViewController.m
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 4/16/13.
//
//

#import "VertexFindViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation VertexFindViewController
@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, discriminant, navigationBar, scrollView,  detailViewController, summaryViewController, mainDetailViewController, showWork ;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        CGRect aRect = self.view.frame;
        aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
            CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
            [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        return NO;
    }
    
    
    return YES;
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    showWork = [[UIBarButtonItem alloc]
                initWithTitle:@"Show Work"
                style:UIBarButtonItemStyleBordered
                target:self
                action:@selector(showWorkView)];
    
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    
    self.aTextField.delegate = self;
    self.bTextField.delegate = self;
    self.cTextField.delegate = self;
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;
    
}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

-(IBAction) completeSquareButtonPressed: (id)sender{
    
    
	a = [aTextField.text doubleValue];
	b = [bTextField.text doubleValue];
	c = [cTextField.text doubleValue];
    if(a == 0  ){
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"The A coefficient is zero!"
                                                           message:@"Please enter in a value other than zero for the A value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
    }
    else if (b == 0 && c == 0) {
        
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"X = 0!"
                                                           message:@"Please enter in at least an A value and either a B or C value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        
    }else{
        
        
        double vertexX = -b/(2*a);
        double vertexY = a*(vertexX*vertexX) + b*vertexX + c;
        discriminant = b*b - 4*a*c;
        
        if (discriminant < 0){
            
            answerTextView.text = [NSString stringWithFormat:@"The vertex is at (%g, %g) and there are no Real zeros", vertexX, vertexY];
        }else{
            xPlus = (-b + sqrt(b*b - 4*a*c))/ (2*a);
            xMin = (-b - sqrt(b*b - 4*a*c))/ (2*a);
            answerTextView.text = [NSString stringWithFormat:@"The vertex is at (%g, %g) and the zeros are at x = %g and x = %g", vertexX, vertexY, xPlus, xMin];
            
        }
        /*
        if (discriminant < 0) {
            answerString = [NSString stringWithFormat: @"<html> <html><style> html {text-align:center; vertical-align:center;} math mtext{mathsize:1.2em; text-align:left} .answer{mathcolor:red;}</style>      <math  style='text-align:left'>    <mtext>To find the vertex of the parabola first use the following equation to find the x-coordinate of the vertex:</mtext></math>   <math> <mrow> <mi>x </mi><mo>= </mo> <mfrac><mi>-b</mi><mi>2a</mi></mfrac></mrow></math>     <mrow>    <mtext>For this equation we have:</mtext></math>      </mrow> <mrow> <mi>x </mi><mo>= </mo> <mfrac><mn>%g</mn><mi>2*%g</mi></mfrac></mrow></math>  </html>",-b, a];
        }else{*/
            answerString = [NSString stringWithFormat: @"<html> <html><style> html {text-align:center; vertical-align:center;} math mtext{mathsize:1.2em; text-align:left} .answer{mathcolor:red;}</style>      <math  style='text-align:left'>    <mtext>To find the vertex of the parabola first use the following equation to find the x-coordinate of the vertex:</mtext><br> <math><mrow> <mi>x </mi><mo>= </mo> <mfrac><mi>-b</mi><mi>2a</mi></mfrac></mrow></math> <br><br>    <mrow>  <mtext>For this equation we have:</mtext></mrow><br><br>        <math><mrow> <mi>x </mi><mo>= </mo> <mfrac><mi>%g</mi><mi>2<mo>&sdot;</mo>%g</mi></mfrac><mo> = </mo><mfrac><mi>%g</mi><mi>%g</mi></mfrac><mo> = </mo><mn style='color:red'>%g</mn></mrow></math><br><br>   <math><mtext>Plugging in the x-value into the original equation we can find the y coordinate of the vertex:</mtext></math>   <math><mi>y </mi> <mo>= </mo><mn>%g</mn><msup><mn style='color:red'>(%g)</mn><mn>2</mn></msup>  <mo>+</mo> <mn>%g</mn><mo>&sdot;</mo><mn style='color:red'>(%g)</mn> <mo>+</mo> <mn>%g</mn></math><br>  <math><mi>y </mi> <mo>= </mo><mn>%g</mn></math><br><br><div style='border:1px red solid;'>%@</div> <br> The zeros were found using the Quadratic Equation as follows   <math><mrow><mi>x</mi><mo>=</mo><mfrac><mrow><mo form='prefix'>&#x2212;<!-- − --></mo><mi>b</mi><mo>&#x00B1;<!-- &PlusMinus; --></mo><msqrt><msup><mi>b</mi><mn>2</mn></msup><mo>&#x2212;<!-- − --></mo><mn>4</mn><mo>&#x2062;<!-- &InvisibleTimes; --></mo><mi>a</mi><mo>&#x2062;<!-- &InvisibleTimes; --></mo><mi>c</mi></msqrt></mrow><mrow><mn>2</mn><mo>&#x2062;<!-- &InvisibleTimes; --></mo><mi>a</mi></mrow></mfrac></mrow></math></html>",-b, a,-b,2*a, vertexX, a, vertexX, b,vertexX, c, vertexY, answerTextView.text];
       // }
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            
            [self.navigationItem setRightBarButtonItem: showWork animated:YES];
        }
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            mainDetailViewController.answerString = answerTextView.text;
            mainDetailViewController.solutionString = answerString;
            [mainDetailViewController.answerWebView setHidden:YES];
            
            [mainDetailViewController configureView];
            
        }
        [self createContentPages];
    }
}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];
    
    NSString *aString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:a]];
    NSString *bString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:b]];
    NSString *cString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:c]];
    
    NSString *contentString = [NSString
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/>With the quadratic equation:  %@x<sup>2</sup> + %@x + %@ <br/> %@ </h2></body></html>",dateString, aString, bString, cString,answerTextView.text ];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [appDelegate.summaryStringsArray addObject:contentString];
       
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];    }
}

-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}


- (void)dealloc {
	/*[aTextField release];
     [bTextField release];
     [cTextField release];
     [super dealloc];*/
}

@end

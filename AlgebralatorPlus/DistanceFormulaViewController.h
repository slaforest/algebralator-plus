//
//  DistanceFormulaViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 3/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"

@class DetailViewController;

@interface DistanceFormulaViewController : UIViewController <UITextFieldDelegate, UISplitViewControllerDelegate> 
{
    
    
    BOOL keyboardVisible;
    UITextField *activeField;
    double x1;
    double y1;
    double x2;
    double y2;
    double distance;
    NSString *answerString;
}
@property (nonatomic, strong) NSMutableArray *modelArray;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *x1TextField;
@property (nonatomic, retain) IBOutlet UITextField *y1TextField;
@property (nonatomic, retain) IBOutlet UITextField *x2TextField;
@property (nonatomic, retain) IBOutlet UITextField *y2TextField;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;
-(IBAction)calculateButtonPressed:(id)sender;
- (void) createContentPages;
-(IBAction)clearButtonPressed:(id)sender;


@end

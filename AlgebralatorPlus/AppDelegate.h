//
//  AppDelegate.h
//  AlgebralatorPlus
//
//  Created by Scott LaForest on 2/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UISplitViewController *splitViewController;
@property (strong, nonatomic) NSMutableString *summaryWebViewString;
@property (strong, nonatomic) NSMutableArray *summaryStringsArray;
@property (readwrite, nonatomic) int solutionCount;
@end

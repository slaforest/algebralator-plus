//
//  MasterViewController.h
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;
@class SummaryViewController;

@interface MasterViewController : UITableViewController <UISearchDisplayDelegate, UISearchBarDelegate>
{
    NSArray *searchedData;
    BOOL isFiltered;
}

@property (nonatomic, retain) NSMutableArray* listOfTasks;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) SummaryViewController *summaryViewController;

@property (nonatomic, assign)  UISplitViewController *splitViewController;

@property (nonatomic, retain) UIPopoverController *popoverController;
@property (nonatomic, retain) UIBarButtonItem *rootPopoverButtonItem;


@property(strong , nonatomic) IBOutlet UISearchBar* searchBar;
@property(strong , nonatomic) IBOutlet UISearchDisplayController* searchDisplay;

-(void)showSummary;
@end

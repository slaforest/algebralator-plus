//
//  ParallelLinesViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"
#import <iAd/iAd.h>
@class DetailViewController;

@interface ParallelLinesViewController : UIViewController <UITextFieldDelegate>
{
	UITextView *instructionsText;
	UITextView *answerTextView;
	UITextField *mTextField;
	UITextField *bTextField;
	UITextField *xTextField;
	UITextField *yTextField;
    UITextField *activeField;

	double m;
	double b;
	double x;
	double y;
	double newB;
	double newM;
    NSString *answerString;

}
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) UITextView *instructionsText;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextField *mTextField;
@property (nonatomic, retain) IBOutlet UITextField *bTextField;
@property (nonatomic, retain) IBOutlet UITextField *xTextField;
@property (nonatomic, retain) IBOutlet UITextField *yTextField;
@property (nonatomic, retain) IBOutlet UILabel *variable1Label;
@property (nonatomic, retain) IBOutlet UILabel *variable2Label;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property(nonatomic, retain)IBOutlet UIScrollView *scrollView;
@property  double m;
@property  double b;
@property  double x;
@property  double y;
@property (nonatomic, retain) IBOutlet UISegmentedControl *lineType;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;
-(IBAction) calculateButtonPressed:(id) sender;

-(IBAction) segmentedControlIndexChanged;
- (void) createContentPages;
-(IBAction)clearButtonPressed:(id)sender;

//-(IBAction)doneEditingButtonPressed:(id)sender;

@end

//
//  Slope2PointsViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "Slope2PointsViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "ShowWorkViewController.h"

@implementation Slope2PointsViewController

@synthesize x1TextField ,x2TextField, y1TextField, y2TextField, answerTextView, directionsTextView, navigationBar, scrollView,  modelArray, mainDetailViewController, detailViewController, summaryViewController, showWork;


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
        if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.3*activeField.frame.origin.y) ;//-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   
    [textField resignFirstResponder];
    return NO;
}


-(IBAction) textFieldDoneEditing:(id)sender{
	
	[sender resignFirstResponder];	
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar c = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        return NO;
    }
    
    
    return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    
    return YES;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
 
    }
    return self;
}
 


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
   
    
    [super viewDidLoad];
    showWork = [[UIBarButtonItem alloc] 
                     initWithTitle:@"Show Work"                                            
                     style:UIBarButtonItemStyleBordered 
                     target:self 
                     action:@selector(showWorkView)];
    
    //self.navigationItem.hidesBackButton = YES;
    [self registerForKeyboardNotifications];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }
    [[self view]setBackgroundColor:[UIColor darkGrayColor]];
    
    self.x1TextField.delegate = self;
    self.x2TextField.delegate = self;
    self.y1TextField.delegate = self;
    self.y2TextField.delegate = self;
 
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];

 // For the border and rounded corners
 [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
 [[answerTextView layer] setBorderWidth:2.3];
 [[answerTextView layer] setCornerRadius:15];
 [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]); 
    
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];

}


#pragma Calculations
-(IBAction)clearButtonPressed:(id)sender{
    
    x1TextField.text = @"";
    x2TextField.text = @"";
    y1TextField.text = @"";
    y2TextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}
-(IBAction)calculateButtonPressed:(id)sender;
{
    
     x1 = [x1TextField.text doubleValue];
     y1 = [y1TextField.text doubleValue];
     x2 = [x2TextField.text doubleValue];
     y2 = [y2TextField.text doubleValue];
    
    
    double deltaX = x2 - x1;
    double deltaY = y2 - y1;
    if (deltaX == 0 && deltaY == 0){
        answerTextView.text = [NSString stringWithFormat:@"Please input two DIFFERENT points. Otherwise the Algebralator cannot compute the slope."];
        
    }else if(deltaX == 0){
        answerTextView.text = [NSString stringWithFormat:@"The slope is undefined"];
    } else if (deltaY == 0){
        answerTextView.text = [NSString stringWithFormat:@"The slope is 0"];

    }
    else{
     slope = deltaY/deltaX;
    answerTextView.text = [NSString stringWithFormat:@"The slope of the line is %g", slope];
    }
   // NSString *answerText = answerTextView.text;
    if (deltaX != 0){
    step1 = [NSString stringWithFormat: @"<html><style>td.upper_line { border-top:solid 1px black; }   table.fraction {width:95%%; text-align: center ; vertical-align: middle;margin-top:0.5em; margin-bottom:0.5em; line-height: 2em; } td.space {width:40%%; text-align:right;  } td.answer {border:solid 2px red; }</style>  <p> To find the slope (<i>m</i>) between two points use the following formula:</p>    <table class='fraction' align='center' cellpadding='0' cellspacing=""0""><tr><td rowspan=""2"" nowrap=""nowrap""><i>m</i> =  </td><td nowrap='nowrap'><i>y</i><sub>2</sub> - <i>y</i><sub>1</sub> </td><td class='space'></td></tr><tr><td class='upper_line'><i>x</i><sub>2</sub> - <i>x</i><sub>1</sub></td></tr></table>   <p> Sub in the coordinate values:</p> <table class='fraction' align='center' cellpadding='0' cellspacing='0'><tr><td rowspan='2' nowrap='nowrap'><i>m</i> =  </td><td nowrap='nowrap'>%g - %g </td><td class='space'></td></tr><tr><td class='upper_line'>%g - %g</td></tr></table> <p> Solving and simplifying to get:</p>   <table class='fraction' align='center' cellpadding='0' cellspacing='0'><tr><td rowspan='2' nowrap='nowrap'><i>m</i> =  </td><td nowrap='nowrap'>%g</td><td rowspan = '2'> = </td> <td class = 'answer' rowspan = '2'>  %g </td></tr><tr><td class='upper_line'>%g</td></tr></table>    </html",y2,y1,x2,x1, deltaY, slope, deltaX];
    }
    else{
          step1 = [NSString stringWithFormat: @"<html><style>td.upper_line { border-top:solid 1px black; }   table.fraction {width:95%%; text-align: center ; vertical-align: middle;margin-top:0.5em; margin-bottom:0.5em; line-height: 2em; } td.space {width:40%%; text-align:right;  } td.answer {border:solid 2px red; }</style>  <p> To find the slope (<i>m</i>) between two points use the following formula:</p>    <table class='fraction' align='center' cellpadding='0' cellspacing=""0""><tr><td rowspan=""2"" nowrap=""nowrap""><i>m</i> =  </td><td nowrap='nowrap'><i>y</i><sub>2</sub> - <i>y</i><sub>1</sub> </td><td class='space'></td></tr><tr><td class='upper_line'><i>x</i><sub>2</sub> - <i>x</i><sub>1</sub></td></tr></table>   <p> Sub in the coordinate values:</p> <table class='fraction' align='center' cellpadding='0' cellspacing='0'><tr><td rowspan='2' nowrap='nowrap'><i>m</i> =  </td><td nowrap='nowrap'>%g - %g </td><td class='space'></td></tr><tr><td class='upper_line'>%g - %g</td></tr></table> <p> Solving and simplifying to get:</p>   <table class='fraction' align='center' cellpadding='0' cellspacing='0'><tr><td rowspan='2' nowrap='nowrap'><i>m</i> =  </td><td nowrap='nowrap'>%g</td><td rowspan = '2'> = </td> <td class = 'answer' rowspan = '2'>  undefined </td></tr><tr><td class='upper_line'>%g</td></tr></table>    </html",y2,y1,x2,x1, deltaY, deltaX];
    }
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = step1;
        [mainDetailViewController.answerWebView setHidden:YES];

        [mainDetailViewController configureView];
        
    }
    [self createContentPages];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

   [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
    }
    slope = 0;
}


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}

- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
   
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
     NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    
    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br /> Slope between two points <br /> ( %g , %g ) and (%g , %g ) <br /> m = %g </h2></body></html>",dateString, x1, y1, x2, y2,slope];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
       //[appDelegate.summaryWebViewString appendFormat:contentString];
           //[summaryViewController configureView];
    }else{
        
       [appDelegate.summaryStringsArray addObject:contentString];
       [mainDetailViewController viewWillAppear:YES];
   }
}
-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = step1;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:step1 baseURL:nil];

}
- (void)dealloc {
   // [super dealloc];
}


@end

//
//  QuadraticFormulaViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "QuadraticFormulaViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation QuadraticFormulaViewController
@synthesize aTextField, bTextField, cTextField, directionsTextView, answerTextView, discriminant, navigationBar, scrollView,  detailViewController, summaryViewController, mainDetailViewController, showWork ;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);//-kbSize.height);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    //NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}



 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
     [super viewDidLoad];

     [self registerForKeyboardNotifications];
     showWork = [[UIBarButtonItem alloc] 
                 initWithTitle:@"Show Work"                                            
                 style:UIBarButtonItemStyleBordered 
                 target:self 
                 action:@selector(showWorkView)];
     


     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

     [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
     scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
     }

     self.aTextField.delegate = self;
     self.bTextField.delegate = self;
     self.cTextField.delegate = self;
     
      [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
     
     // For the border and rounded corners
     [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[answerTextView layer] setBorderWidth:2.3];
     [[answerTextView layer] setCornerRadius:15];
     [answerTextView setClipsToBounds: YES];
     //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
     // For the border and rounded corners
     [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[directionsTextView layer] setBorderWidth:2.3];
     [[directionsTextView  layer] setCornerRadius:15];
     [directionsTextView setClipsToBounds: YES];

 }
 
 

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];

    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}

-(IBAction) completeSquareButtonPressed: (id)sender{

   
	a = [aTextField.text doubleValue];
	b = [bTextField.text doubleValue];
	c = [cTextField.text doubleValue];
    
    if(a == 0  ){
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"A coefficient is zero!"
                                                           message:@"Please enter in a value other than zero for the A value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
    }  
    else if (b == 0 && c == 0) {
        
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"X = 0!"
                                                           message:@"Please enter in at least an A value and either a B or C value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        
    }else{
	NSLog(@"%lf %lf %lf", a ,b ,c);
    discriminant = b*b - 4*a*c;
        NSLog(@"Discriminatn = %g", discriminant);
    if (discriminant < 0){
       /* discriminant = abs(discriminant);
        double RealPart = -b/2*a ;
        double imaginaryCoeff = sqrt(discriminant)/ 2*a;
            
        NSString* xPlusImaginary = [NSString stringWithFormat: @"%g +  %g%@", RealPart, imaginaryCoeff, i];
            NSString* xMinusImaginary  = [NSString stringWithFormat: @"%g - %g%@", RealPart, imaginaryCoeff,i];
            answerTextView.text = [NSString stringWithFormat:@"x = %@ or x = %@", xPlusImaginary,xMinusImaginary];*/
                answerTextView.text = @"No Real Solution";
            }else{
    xPlus = (-b + sqrt(b*b - 4*a*c))/ (2*a);
	xMin = (-b - sqrt(b*b - 4*a*c))/ (2*a);
	answerTextView.text = [NSString stringWithFormat:@"x = %g or x = %g", xPlus, xMin];
    //answerTextView.text = [NSString stringWithFormat:@"(x + %g)(x + %g)", -1*xPlus, -1*xMin];

	}
        if (discriminant < 0) {
            answerString = [NSString stringWithFormat: @"<html><style>td.upper_line { border-top:solid 1px black; }   table.fraction {width:95%%; text-align: center ; vertical-align: middle;margin-top:0.5em; margin-bottom:0.5em; line-height: 2em; } td.sqrt{text-align:right;vertical-align:top; } td.space {width:25%%; text-align:right;  } td.answer {border:solid 2px red; }</style><p>To find the solutions of a quadratic equation we can use the Quadratic Formula:</p><table><tr><td nowrap = 'nowrap' rowspan='2'>x = </td><td>&minus;b &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;b<span style='font-size: 10px;vertical-align:+50%%;'>2</span> &minus;4ac&nbsp;</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2a &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></td></tr></table>                                  <p>Subbing in the A,B, and C values</p><table><tr><td rowspan='2' nowrap='nowrap'>x = </td><td nowrap='nowrap'>&minus;%g &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;%g<span style='font-size: 10px;vertical-align:+50%%;'>2</span> &minus;4(%g)(%g)&nbsp;</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2(%g) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></td></tr></table>           <table><tr><td rowspan='2'> = </td><td nowrap='nowrap'>&minus;%g &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;%g</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;%g &nbsp; &nbsp; &nbsp;</span></td></tr></table>                <table><tr><td class='answer'>%@</td></tr></table></html>",b,b,a,c,a,b,discriminant,a*2,answerTextView.text ];
        }else{
            answerString = [NSString stringWithFormat: @"<html><style>td.upper_line { border-top:solid 1px black; }   table.fraction {width:95%%; text-align: center ; vertical-align: middle;margin-top:0.5em; margin-bottom:0.5em; line-height: 2em; } td.sqrt{text-align:right;vertical-align:top; } td.space {width:30%%; text-align:right;  } td.answer {border:solid 2px red; }</style><p>To find the solutions of a quadratic equation we can use the Quadratic Formula:</p><table><tr><td nowrap = 'nowrap' rowspan='2;'>x = </td><td nowrap = 'nowrap'>&minus;b &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;b<span style='font-size: 10px;vertical-align:+50%%;'>2</span> &minus;4ac&nbsp;</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2a &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></td></tr></table>                                  <p>Subbing in the A,B, and C values</p><table><tr><td rowspan='2' nowrap='nowrap'>x = </td><td nowrap='nowrap'>%g &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;%g<span style='font-size: 10px;vertical-align:+50%%;'>2</span> &minus;4(%g)(%g)&nbsp;</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2(%g) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></td></tr></table>           <table><tr><td rowspan='2'> = </td><td nowrap = 'nowrap'>%g &plusmn;&radic;<span style='text-decoration:overline'> &nbsp;%g</span></td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; %g &nbsp; &nbsp; &nbsp;</span></td></tr></table>                <table><tr><td rowspan='2'> x = </td><td style='text-align:center;'>%g + %g</td><td rowspan='2'> or </td><td style='text-align:center;'> %g - %g</td></tr><tr><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp; %g &nbsp; &nbsp; &nbsp;</span></td><td style='text-align:center;'><span style='text-decoration:overline;'>&nbsp; &nbsp; &nbsp;%g &nbsp; &nbsp; &nbsp;</span></td></tr></table>                                 <table><tr><td class='answer'>%@</td></tr></table></html>",-1*b,-1*b,a,c,a,-1*b,discriminant,a*2,-1*b,sqrt(discriminant), -1*b, sqrt(discriminant),2*a,2*a,answerTextView.text ];
        }          

        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            
            [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
        }
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            mainDetailViewController.answerString = answerTextView.text;
            mainDetailViewController.solutionString = answerString;
            [mainDetailViewController.answerWebView setHidden:YES];

            [mainDetailViewController configureView];
            
        }
        [self createContentPages];
    }
}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];

    NSString *aString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:a]];
    NSString *bString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:b]];
    NSString *cString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:c]];

    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/>Solutions of  the quadratic equation:  %@x<sup>2</sup> + %@x + %@<br/> %@ </h2></body></html>",dateString, aString, bString, cString,answerTextView.text ];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
[mainDetailViewController viewWillAppear:YES];    }
}

-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}


- (void)dealloc {
	/*[aTextField release];
	[bTextField release];
	[cTextField release];
    [super dealloc];*/
}

@end

//
//  FractionsViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 1/15/12.
//  Copyright (c) 2012 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"
#import <iAd/iAd.h>

@class DetailViewController;

@interface FractionsViewController : UIViewController <UITextFieldDelegate>
{
    UITextField *activeField;
    int aDenominatorValue ;
    int bDenominatorValue ;
    int aNumeratorValue ;
    int bNumeratorValue;
    int finalNumerator;
    int aNumeratorCommon;
    int bNumeratorCommon;
    int bDenominatorCommon;
    int aDenominatorCommon;
    int finalDenominator;
    
    int aMultiplier;
    int bMultiplier;
    int commonDenominator;
    NSString *answerString;

}
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *aNumerator;
@property (nonatomic, retain) IBOutlet UITextField *aDenominator;
@property (nonatomic, retain) IBOutlet UITextField *bNumerator;
@property (nonatomic, retain) IBOutlet UITextField *bDenominator;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *lineType;
@property (nonatomic, retain) IBOutlet UILabel *operationLabel;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;
- (void) createContentPages;

-(IBAction) segmentedControlIndexChanged;
-(IBAction)calculateButtonPressed:(id)sender;
-(IBAction)clearButtonPressed:(id)sender;

@end

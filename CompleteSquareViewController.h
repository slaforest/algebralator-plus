//
//  CompleteSquareViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 8/3/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"

@class DetailViewController;

@interface CompleteSquareViewController : UIViewController <UITextFieldDelegate>{
    

@private
	UITextField *aTextField;
	UITextField *bTextField;
	UITextField *cTextField;
	UITextView *answer;
    UITextField *activeField;

	double aValue;
	double bValue;
	double cValue;
    NSString *step1;
    NSString  *step2;
    NSString    *finalAnswer;
    NSString *answerString;

}
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;

@property (nonatomic, retain) IBOutlet UITextField *aTextField;
@property (nonatomic, retain) IBOutlet UITextField *bTextField;
@property (nonatomic, retain) IBOutlet UITextField *cTextField;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;
- (void) createContentPages;

- (IBAction) completeSquareButtonPressed: (id) sender;
-(IBAction) textFieldDoneEditing: (id)sender;
-(IBAction)clearButtonPressed:(id)sender;

@end

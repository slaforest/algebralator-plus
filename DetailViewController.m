//
//  DetailViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "SummaryViewController.h"

@interface DetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation DetailViewController

@synthesize detailItem = _detailItem;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize dataObject, solutionString, answerString, webView, paperView, summaryButton, detailAnswerView ;


#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
       [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}
-(void)resetView{
    [self.webView loadHTMLString:@"" baseURL:nil];
    [self.answerWebView setHidden:YES];
   // self.detailAnswerView.text = @"";
    
}

- (void)configureView
{
    NSLog(@"anserstring = %@", answerString);
    [self.webView loadHTMLString:solutionString baseURL:nil];
    self.detailAnswerView.text = answerString;
    
    // Update the user interface for the detail item.
    
    if (self.detailItem) {
        NSLog(@"anserstring = %@", answerString);
        
        
        
    }
}
-(void) viewWillAppear:(BOOL)animated{
    //[self moveBannerOffscreen];
   // CGRect newScrollFrame = self.scrollView.frame;
   // newScrollFrame.origin.y = 70;
   // if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //self.solutionString = @"";
        [self configureView];
   // }
           

    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   // CGRect newScrollFrame = self.scrollView.frame;
   // newScrollFrame.origin.y = 70;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {

    summaryButton = [[UIBarButtonItem alloc] 
                                      initWithTitle:@"Summary"                                            
                                      style:UIBarButtonItemStyleBordered 
                                      target:self 
                                      action:@selector(showSummary)];
    [self.navigationItem setRightBarButtonItem:summaryButton];
    }
    [[detailAnswerView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[detailAnswerView layer] setBorderWidth:2.3];
    [[detailAnswerView layer] setCornerRadius:15];
    [detailAnswerView setClipsToBounds: YES];

    [[_answerWebView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[_answerWebView layer] setBorderWidth:2.3];
    [[_answerWebView layer] setCornerRadius:15];
    [_answerWebView setClipsToBounds: YES];
    // webViewString = [[NSMutableString alloc] initWithString: @""];
   // appDelegate =(AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    
}
    

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
   // self.webViewString = nil;
   // self.solutionCount = 0;
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}



- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
    //self.webViewString = nil;
    //self.solutionCount = 0;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
            return YES;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Algebralator Plus", @"Algebralator Plus");
    }
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Functions", @"Functions");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}
-(void)showSummary{
    
    SummaryViewController *summaryViewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController_iPad" bundle:nil];
    summaryViewController.navigationItem.title = @"Summary";
    
    [self.navigationController pushViewController:summaryViewController animated:YES];
}


@end

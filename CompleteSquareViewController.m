//
//  CompleteSquareViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/3/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "CompleteSquareViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation CompleteSquareViewController
@synthesize aTextField, bTextField, cTextField, answerTextView, directionsTextView, navigationBar,scrollView, summaryViewController, detailViewController, mainDetailViewController, showWork;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}
-(void) viewWillAppear:(BOOL)animated{
    
}

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}



# pragma Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only numeric values, a decimal point, or a negative sign(-)"
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    [self registerForKeyboardNotifications];
    showWork = [[UIBarButtonItem alloc] 
                initWithTitle:@"Show Work"                                            
                style:UIBarButtonItemStyleBordered 
                target:self 
                action:@selector(showWorkView)];


    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
    scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
    }

    self.aTextField.delegate = self;
    self.bTextField.delegate = self;
    self.cTextField.delegate = self;

    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    
    // For the border and rounded corners
    [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[answerTextView layer] setBorderWidth:2.3];
    [[answerTextView layer] setCornerRadius:15];
    [answerTextView setClipsToBounds: YES];
    //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
    // For the border and rounded corners
    [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
    [[directionsTextView layer] setBorderWidth:2.3];
    [[directionsTextView  layer] setCornerRadius:15];
    [directionsTextView setClipsToBounds: YES];
    
}



- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];

    /*[self.toolBar setHidden:YES];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        mainDetailViewController.detailAnswerView.text = @"";
        mainDetailViewController.navigationItem.rightBarButtonItem = nil;
        [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
    }*/
}


-(IBAction) completeSquareButtonPressed: (id)sender{
	NSLog(@"calc button pressed");
   
//Unicode: U+00B2, UTF-8: C2 B2
	
	aValue = [aTextField.text doubleValue];
	bValue = [bTextField.text doubleValue];
	cValue = [cTextField.text doubleValue];
	NSLog(@"%g %g %g",aValue, bValue, cValue);
    
    if(aValue == 0  ){
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"A coefficient is zero!"
                                                           message:@"Please enter in a value other than zero for the A value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
    }  
    else if (bValue == 0 && cValue == 0) {
        
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"X = 0!"
                                                           message:@"Please enter in at least an A value and either a B or C value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        
    }else{
	NSString *squared = @"\u00B2";
	bValue = bValue/aValue;;
	cValue = cValue/aValue;
	aValue = aValue/aValue ;
    double cValueOriginal = cValue;
    double halfB = bValue/2;
	double halfBSquare = (bValue/2)*(bValue/2) ;
	cValue = -cValue + halfBSquare;
	double xPlus = (-bValue/2) + sqrt(cValue) ;
	double xMin =  (-bValue/2) - sqrt(cValue) ;
	step2 = [NSString stringWithFormat:@" (x + %g)%@ = %g \n", halfB,squared, cValue];
    
	if (cValue < 0){
        finalAnswer = [NSString stringWithFormat:@"No Real Number Solution"];
    } else{
        finalAnswer = [NSString stringWithFormat:@"x = %g or x = %g \n", xPlus, xMin];
        }
	step1 = [NSString stringWithFormat:@" x%@ + %gx + %g = %g \n",squared, bValue, halfBSquare, cValue];
    NSString *step1And2 = [step1 stringByAppendingString :step2];
    answerTextView.text = step1;
    answerTextView.text = step1And2;
	answerTextView.text = [step1And2 stringByAppendingString:finalAnswer];
    
        if (cValue  < 0){
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:30%%; text-align:right;} td.answer {border:solid 2px red; }   </style>  <p>To complete the square we first get all the constants on the right side of the equation by adding their opposite and if the A value is not 1, we need to divide all terms by the A value</p>   <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx + %g <font color='blue'> + %g  <font color='black'> = <font color='blue'> %g </span></td></td></tr></table>       <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx  &nbsp; &nbsp;   =  %g </span></td></tr></table>      <p>Now take half of the b value <font color='black'>(%g x .5 = %g) <font color='black'>, square it <font color='blue'> (%g<sup>2</sup> = %g)<font color='black'>, and add it to BOTH sides.</p>    <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx <font color='blue'> + %g  <font color='black'> = %g <font color='blue'> + %g </td></tr></table>    <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx + %g   =  %g </td></tr></table> <p> Factoring the left side gives us </p>   <table><tr><td nowrap = 'nowrap'>%@</td></tr></table>   <p> Normally we would now take the square root of both sides but since the right side is negative there is no real solution</p>      <table><tr><td class='answer'> %@</td></tr.</table></html>", aValue, bValue, cValueOriginal, -cValueOriginal, -cValueOriginal, aValue, bValue, -cValueOriginal,bValue, halfB, halfB, halfBSquare, aValue, bValue, halfBSquare, -cValueOriginal, halfBSquare, aValue, bValue, halfBSquare, cValue, step2,  finalAnswer ];        
            
        }else{
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:30%%; text-align:right;} td.answer {border:solid 2px red; } td.numbering {text-align: right; }  </style>  <p>To complete the square we first get all the constants on the right side of the equation by adding their opposite and if the A value is not 1, we need to divide all terms by the A value</p>   <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx + %g <font color='blue'> + %g  <font color='black'> = <font color='blue'> %g </td></tr></table>       <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx  &nbsp; &nbsp;   =  %g </span></td></tr></table>      <p>Now take half of the b value <font color='black'>(%g x .5 = %g) <font color='black'>, square it <font color='blue'> (%g<sup>2</sup> = %g)<font color='black'>, and add it to BOTH sides.</p>    <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx <font color='blue'> + %g  <font color='black'> = %g <font color='blue'> + %g </td></tr></table>    <table><tr><td nowrap = 'nowrap'>%gx<sup>2</sup> + %gx + %g   =  %g </td></tr></table> <p> Factoring the left side gives us </p>   <table><tr><td nowrap = 'nowrap'>%@</td</tr></table>   <p> To solve, take the square root of both sides resulting in </p>     <table><tr><td nowrap = 'nowrap'> x + %g = &plusmn; %g</tr></table>       <p> Solving for x </p> <table><tr><td class='answer'> %@</td></tr.</table></html>", aValue, bValue, cValueOriginal, -cValueOriginal, -cValueOriginal, aValue, bValue, -cValueOriginal,bValue, halfB, halfB, halfBSquare, aValue, bValue, halfBSquare, -cValueOriginal, halfBSquare, aValue, bValue, halfBSquare, cValue, step2, halfB, sqrt(cValue), finalAnswer ];        
        }
    
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            
            [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
        }
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            mainDetailViewController.answerString = answerTextView.text;
            mainDetailViewController.solutionString = answerString;
            [mainDetailViewController.answerWebView setHidden:YES];

            [mainDetailViewController configureView];
            
        }
        [self createContentPages];
    }
}
- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];
    
    NSString *aString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:aValue]];
    NSString *bString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:bValue]];
    NSString *cString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat: [cTextField.text doubleValue]]];

    
    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/>By completing the square the solutions of %@x<sup>2</sup> + %@x + %@ are<br/> %@ <br/> %@<br/>  %@</h2></body></html>",dateString, aString, bString, cString,step1, step2, finalAnswer];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];    }
}

-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}


- (void)dealloc {
	/*[aTextField release];
	[bTextField release];
	[cTextField release];
    [super dealloc];*/
}


@end

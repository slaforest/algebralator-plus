//
//  MasterViewController.m
//  AlgebralatorUniversal
//
//  Created by Scott LaForest on 2/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MasterViewController.h"
#import "Slope2PointsViewController.h"
#import "DetailViewController.h"
#import "ParallelLinesViewController.h"
#import "PerpendicularLinesViewController.h"
#import "FractionsViewController.h"
#import "MultiplyFractionsViewController.h"
#import "QuadraticFormulaViewController.h"
#import "CompleteSquareViewController.h"
#import "FactorQuadraticsViewController.h"
#import "SummaryViewController.h"
#import "DistanceFormulaViewController.h"
#import "DecimalsToFractionsViewController.h"
#import "PythagoreanViewController.h"
#import "RightTriangleCheckerViewController.h"
#import "OneMatrixViewController.h"
#import "TwoMatrixViewController.h"
#import "VertexFindViewController.h"
#import "SolveMatricesViewController.h"
#import "SyntheticDivisionViewController.h"

@implementation MasterViewController

@synthesize detailViewController = _detailViewController, listOfTasks, mainDetailViewController, popoverController, rootPopoverButtonItem, splitViewController, summaryViewController;

-(void)showSummary{
        summaryViewController = [[SummaryViewController alloc] initWithNibName:@"SummaryViewController" bundle:nil];
        summaryViewController.navigationItem.title = @"Summary";
    
    [self.navigationController pushViewController:summaryViewController animated:YES];

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Functions", @"Functions");
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            self.clearsSelectionOnViewWillAppear = NO;
            self.contentSizeForViewInPopover = CGSizeMake(320.0, 600);
        }
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    
    
        UIBarButtonItem *summaryButton = [[UIBarButtonItem alloc] 
                                       initWithTitle:@"Summary"                                            
                                       style:UIBarButtonItemStyleBordered 
                                       target:self 
                                       action:@selector(showSummary)];
        self.navigationItem.rightBarButtonItem = summaryButton;    
    }
    
    [self.tableView setBackgroundView:nil];
    
    //mainDetailViewController.navigationItem.rightBarButtonItem = nil;
    
    self.navigationItem.title = @"Functions";
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                     style:UIBarButtonItemStyleBordered
                                    target:nil
                                    action:nil] ;
    
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
    //[[self view] set
	listOfTasks = [[NSMutableArray alloc] init];
	NSArray *linearTasksArray = [NSArray arrayWithObjects:@"Slope between 2 points",@"Parallel lines", @"Perpendicular lines",@"Distance Formula", nil];
	NSDictionary *linearTasksDict = [NSDictionary dictionaryWithObject:linearTasksArray forKey:@"Tasks"];
	
    NSArray *fractionsTasksArray = [NSArray arrayWithObjects:@"Add or Subtract Fractions", @"Multiply or Divide Fractions" ,@"Decimals to Fractions", nil];
    
	NSDictionary *fractionsTasksDict = [NSDictionary dictionaryWithObject:fractionsTasksArray forKey:@"Tasks"];
    
    
	NSArray *quadraticTasksArray = [NSArray arrayWithObjects:@"Solve with Quadratic Formula", @"Complete the square", @"Factor quadratics",@"Dude, where's my vertex?", nil];
	NSDictionary *quadraticTasksDict = [NSDictionary dictionaryWithObject:quadraticTasksArray forKey:@"Tasks"];
    NSArray *pythagoreanTasksArray = [NSArray arrayWithObjects:@"Solve Pythagorean Theorem", @"Right Triangle Checker", nil];
	NSDictionary *pythagoreanTasksDict = [NSDictionary dictionaryWithObject:pythagoreanTasksArray forKey:@"Tasks"];
    NSArray *matricesTasksArray = [NSArray arrayWithObjects:@"Square Matrix", @"2 Matrices",@"Solve Matrices", nil];
	NSDictionary *matricesTasksDict = [NSDictionary dictionaryWithObject:matricesTasksArray forKey:@"Tasks"];
    //NSArray* polynomialTasksArray = [NSArray arrayWithObjects:@"Synthetic Division", nil];
   // NSDictionary *polynomialTasksDict = [NSDictionary dictionaryWithObject:polynomialTasksArray forKey:@"Tasks"];
	[listOfTasks addObject:linearTasksDict];
    [listOfTasks addObject:fractionsTasksDict];
	[listOfTasks addObject:quadraticTasksDict];
    [listOfTasks addObject:pythagoreanTasksDict];
    [listOfTasks addObject:matricesTasksDict];
    //[listOfTasks addObject: polynomialTasksDict];

	// Do any additional setup after loading the view, typically from a nib.
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];
    [mainDetailViewController.navigationController popToRootViewControllerAnimated:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
           return YES;
    
}

#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return isFiltered ? 1 :[listOfTasks count];
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	NSDictionary *dictionary = [listOfTasks objectAtIndex:section];
	NSArray *array = [dictionary objectForKey:@"Tasks"];
	return isFiltered ? searchedData.count :[array count];
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if (section == 0){
		return isFiltered ? nil: @"Linear Equations";
	}else if (section == 1) {
		return isFiltered ? nil: @"Fractions";
	}else if (section == 2){
		return isFiltered ? nil: @"Quadratic Equations";
	}else if(section == 3){
        return isFiltered ? nil: @"Pythagorean Theorem";
    }else if(section == 4){
        return isFiltered ? nil: @"Matrices";
    }else{
        return isFiltered ? nil: @"Polynomials";
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView titleForHeaderInSection:section] != nil) {
        return 35;
    }
    else {
        // If no section header title, no section header needed
        return 0;
    }
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
    if (sectionTitle == nil) {
        return nil;
    }
    
    // Create label with section title
    UILabel *label = [[UILabel alloc] init] ;
    label.frame = CGRectMake(20, 6, 300, 30);
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor greenColor];
    label.shadowColor = [UIColor greenColor];
    label.shadowOffset = CGSizeMake(0.0, 1.0);
    label.font = [UIFont fontWithName:@"Futura-CondensedExtraBold" size:22];
    label.text = sectionTitle;
    
    // Create header view and add label as a subview
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    
    [view addSubview:label];
    
    return view;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    
	// Configure the cell.
	NSDictionary *dictionary = [listOfTasks objectAtIndex:indexPath.section];
	NSArray *array = [dictionary objectForKey:@"Tasks"];
	
	cell.textLabel.text = cell.textLabel.text = isFiltered ? searchedData[indexPath.row] : [array objectAtIndex:indexPath.row];;
	cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;

	
    
}

#pragma mark - SearchBar Delegate -
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    if (searchText.length == 0)
        isFiltered = NO;
    else
        isFiltered = YES;
    
    NSMutableArray *tmpSearched = [[NSMutableArray alloc] init];
    NSMutableArray *tmpDict = [[NSMutableArray alloc] init];
    NSMutableArray *justTheTerms = [[NSMutableArray alloc]init ];
    for (NSDictionary* section in listOfTasks) {
        NSArray *array = [section objectForKey:@"Tasks"];
        for (NSString* function in array) {
            [justTheTerms addObject: function];
        }
        /*for (int i = 0; i < section.count; i++) {
         [justTheTerms addObject:[section objectAtIndex:i]];
         }*/
        
    }
    
    for (NSString* string in justTheTerms) {
        
        //we are going for case insensitive search here
        NSRange range = [string rangeOfString:searchText
                                      options:NSCaseInsensitiveSearch];
        
        if (range.location != NSNotFound && ![tmpSearched containsObject:string])
            [tmpSearched addObject:string];
    }
    for (int k = 0;k<justTheTerms.count; k++) {
        for (int l = 0; l <tmpSearched.count; l++) {
            if ([[justTheTerms objectAtIndex:k] isEqualToString:[tmpSearched objectAtIndex:l]]) {
                [tmpDict addObject:justTheTerms[k]];
            }
        }
        
        
    }
    searchedData = tmpDict.copy;
    
    [self.tableView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
}
-(void) searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    isFiltered = NO;
    [self.tableView reloadData];
}


#pragma mark -
#pragma mark Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int myRow = 0;
    int mySection = 0;
    
    if (isFiltered) {
        for (int i = 0; i<listOfTasks.count;i++) {
            NSDictionary* sections = [listOfTasks objectAtIndex:i];
            NSArray* keys = [sections allKeys];
            for (int j = 0; j<keys.count;j++) {
                NSString* function = [keys objectAtIndex:j];
                if ([function isEqualToString: [searchedData objectAtIndex:indexPath.row]]) {
                    myRow = j;
                    mySection = i;
                }
            }
        }
    }else{
        mySection = indexPath.section;
        myRow = indexPath.row;
    }

    if(myRow == 0 && mySection == 0){
        Slope2PointsViewController *slopeController = [[Slope2PointsViewController alloc] initWithNibName:@"Slope2PointsViewController" bundle:nil];
            slopeController.navigationItem.title = @"Slope Between 2 Points";
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            //slopeController.summaryViewController = self.summaryViewController;
            self.mainDetailViewController.solutionString = nil;
        }else{
        
            slopeController.mainDetailViewController = self.mainDetailViewController;
        }
            [self.navigationController pushViewController:slopeController animated:YES];
            [slopeController viewWillAppear:YES];
        
       
        
	}
      else if (myRow == 1 && mySection == 0) {
      ParallelLinesViewController *detailViewController = [[ParallelLinesViewController alloc] initWithNibName:@"ParallelLinesViewController" bundle:nil];
          
          detailViewController.navigationItem.title = @"Parallel Lines";
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }	 
      else if (myRow == 2 && mySection == 0) {
      PerpendicularLinesViewController *detailViewController = [[PerpendicularLinesViewController alloc] initWithNibName:@"PerpendicularLinesViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Perpendicular Lines";

          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
             // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];

      }else if (myRow == 3 && mySection == 0) {
          DistanceFormulaViewController *detailViewController = [[DistanceFormulaViewController   alloc] initWithNibName:@"DistanceFormulaViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Distance Between 2 Points";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
          
      }

      else if (myRow == 0 && mySection == 1) {
      FractionsViewController *detailViewController = [[FractionsViewController alloc] initWithNibName:@"FractionsViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Add or Subtract Fractions";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
             // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
          

      }
      else if (myRow == 1 && mySection == 1) {
      MultiplyFractionsViewController *detailViewController = [[MultiplyFractionsViewController alloc] initWithNibName:@"MultiplyFractionsViewController" bundle:nil];
        detailViewController.navigationItem.title = @"Multiply/Divide Fractions";
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
      //  detailViewController.summaryViewController = self.summaryViewController;
        self.mainDetailViewController.solutionString = nil;
        }else{
        
        detailViewController.mainDetailViewController = self.mainDetailViewController;
        }
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController viewWillAppear:YES];
        
        
      }else if (myRow == 2 && mySection == 1) {
          DecimalsToFractionsViewController  *detailViewController = [[DecimalsToFractionsViewController alloc] initWithNibName:@"DecimalsToFractionsViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Decimals to Fractions";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //  detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
          
          
      }

      
      else if (myRow == 0 && mySection == 2) {
      QuadraticFormulaViewController *detailViewController = [[QuadraticFormulaViewController alloc] initWithNibName:@"QuadraticFormulaViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Quadratic Formula";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            //  detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
          
      }
      else if (myRow == 1 && mySection == 2) {
      CompleteSquareViewController *detailViewController = [[CompleteSquareViewController	alloc] initWithNibName:@"CompleteSquareViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Complete the Square";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];

      }
      else if (myRow == 2 && mySection == 2) {
      FactorQuadraticsViewController *detailViewController = [[FactorQuadraticsViewController alloc] initWithNibName:@"FactorQuadraticsViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Factor Quadratics";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
             // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }
      else if (myRow == 3 && mySection == 2) {
          VertexFindViewController *detailViewController = [[VertexFindViewController alloc] initWithNibName:@"QuadraticFormulaViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Vertex Finder";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }

    
      else if (myRow == 0 && mySection == 3) {
          PythagoreanViewController *detailViewController = [[PythagoreanViewController alloc] initWithNibName:@"PythagoreanViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Solve Pythagorean Thm.";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }
      else if (myRow == 1 && mySection == 3) {
          RightTriangleCheckerViewController *detailViewController = [[RightTriangleCheckerViewController alloc] initWithNibName:@"RightTriangleCheckerViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Right Triangle Checker";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              // detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }
      else if (myRow == 0 && mySection == 4) {
          OneMatrixViewController  *detailViewController = [[OneMatrixViewController alloc] initWithNibName:@"OneMatrixViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Square Matrix";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }

      else if (myRow == 1 && mySection == 4) {
          TwoMatrixViewController  *detailViewController = [[TwoMatrixViewController alloc] initWithNibName:@"TwoMatrixViewController" bundle:nil];
          detailViewController.navigationItem.title = @"2 Matrices";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];
      }
      else if (myRow == 2 && mySection == 4) {
          SolveMatricesViewController  *detailViewController = [[SolveMatricesViewController alloc] initWithNibName:@"SolveMatricesViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Solve Matrices";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          //[detailViewController viewWillAppear:YES];
      }
      else if (myRow == 0 && mySection == 5) {
           SyntheticDivisionViewController *detailViewController = [[SyntheticDivisionViewController alloc] initWithNibName:@"SyntheticDivisionViewController" bundle:nil];
          detailViewController.navigationItem.title = @"Synthetic Division";
          
          if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
              //detailViewController.summaryViewController = self.summaryViewController;
              self.mainDetailViewController.solutionString = nil;
          }else{
              
              //detailViewController.mainDetailViewController = self.mainDetailViewController;
          }
          [self.navigationController pushViewController:detailViewController animated:YES];
          [detailViewController viewWillAppear:YES];

      }

}
@end

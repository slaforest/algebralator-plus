//
//  FactorQuadraticsViewController.m
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import "FactorQuadraticsViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "ShowWorkViewController.h"

@implementation FactorQuadraticsViewController
@synthesize aTextField, bTextField, cTextField,directionsTextView, answerTextView,navigationBar, scrollView, detailViewController,mainDetailViewController,summaryViewController, showWork ;

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}
// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.height - 2.4*activeField.frame.origin.y);
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
 if (self) {
 // Custom initialization.
 }
 return self;
 }
 */

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return YES;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    }

- (void) viewWillDisappear:(BOOL)animated {
	[super viewWillAppear:animated];
    
		//NSLog(@"%@", @"Unregistering for keyboard events...");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}




#pragma mark-
# pragma mark Controlling UI, user input, and keyboard

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(IBAction) textFieldDoneEditing:(id)sender{
	
    
	[sender resignFirstResponder];	
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    NSString *currentString = textField.text;
    if ([string isEqualToString:@""]) return YES;
    
    //if ([currentString rangeOfString:@"."].location == NSNotFound && [string isEqualToString:@"."]) return YES;
    
    if (currentString.length == 0 && [string isEqualToString:@"-"]) return YES;
    
    unichar ch = [string characterAtIndex:0];
    if ([[NSCharacterSet decimalDigitCharacterSet] characterIsMember:ch]) {
        return YES;
    } else {
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"Invalid Input"
                                                           message:@"Please enter in only integer values. If you need to solve a quadratic with non-integer values, try using the Quadratic Formula."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];

        return NO;
    }
    
    
    return YES;
}


 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad {
     [super viewDidLoad];

     [self registerForKeyboardNotifications];
     showWork = [[UIBarButtonItem alloc] 
                 initWithTitle:@"Show Work"                                            
                 style:UIBarButtonItemStyleBordered 
                 target:self 
                 action:@selector(showWorkView)];

     
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {

     [scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height+150)];
     scrollView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleWidth;
     
     }
     self.aTextField.delegate = self;
     self.bTextField.delegate = self;
     self.cTextField.delegate = self;
     
    [[self view]setBackgroundColor:[UIColor colorWithRed:0.43921568627451 green:0.50196078431373 blue:0.56470588235294 alpha:1.0]];
     
     // For the border and rounded corners
     [[answerTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[answerTextView layer] setBorderWidth:2.3];
     [[answerTextView layer] setCornerRadius:15];
     [answerTextView setClipsToBounds: YES];
     //NSLog(@"%@", [UIFont fontNamesForFamilyName:@"Futura"]);
     // For the border and rounded corners
     [[directionsTextView layer] setBorderColor:[[UIColor greenColor] CGColor]];
     [[directionsTextView layer] setBorderWidth:2.3];
     [[directionsTextView  layer] setCornerRadius:15];
     [directionsTextView setClipsToBounds: YES];


 }
 

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.navigationBar = nil;

}
-(IBAction)clearButtonPressed:(id)sender{
    
    aTextField.text = @"";
    bTextField.text = @"";
    cTextField.text = @"";
    answerTextView.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    mainDetailViewController.answerString = @"";
    mainDetailViewController.solutionString = @"";
    [mainDetailViewController configureView];

    /*[self.toolBar setHidden:YES];
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
     mainDetailViewController.detailAnswerView.text = @"";
     mainDetailViewController.navigationItem.rightBarButtonItem = nil;
     [mainDetailViewController.solutionWebView loadHTMLString:@"" baseURL:nil];
     }*/
}


-(IBAction) factorButtonPressed: (id)sender{
   	
	
    
    factorsOfA = [[NSMutableArray alloc] init];
	factorsOfC = [[NSMutableArray alloc] init];
    
    
	a = [aTextField.text intValue];
	b = [bTextField.text intValue];
	c = [cTextField.text intValue];
    BOOL notFactored = NO;
    BOOL negativeFactor;
    BOOL isBinomial = NO;
    // abs value A and C in order to get factors easier. Will handle sign change later.
    aAbs = abs((int)a);
    bAbs = abs((int) b);
    cAbs = abs((int)c);
    NSLog(@"%d %d %d", a ,b ,c);
    
    
    if(a == 0  ){
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"A coefficient is zero!"
                                                           message:@"Please enter in a value other than zero for the A value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
    }  
    else if (b  == 0 && c  == 0) {
        
        UIAlertView *badInput = [[UIAlertView alloc] initWithTitle:@"X = 0!"
                                                           message:@"Please enter in at least an A value and either a B or C value."
                                                          delegate:nil
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:nil];
        [badInput show];
        
        
    }else if(a<0 && c != 0){
            a *= -1;
            b *= -1;
            c *= -1;
        negativeFactor = YES;
        }
    //else{
    //}
    
    
	
	
    
	//handle situation where C is 0.
    if (c == 0){
        isBinomial = YES;
        if (a != 1) {
            
            
            for (int i = 1; i <= aAbs; i++){
                if(aAbs % i == 0){
                    NSNumber *factora1 = [[NSNumber alloc] initWithInt : i];
                    NSNumber *factora2 = [[NSNumber alloc] initWithInt: (aAbs/i)];
                    if(factora1 == factora2){
                        [factorsOfA addObject: factora1];
                        [factorsOfA addObject: factora2];
                    }
                    else if(![factorsOfA containsObject:factora1]){
                        [factorsOfA addObject: factora1];
                    }else if(![factorsOfA containsObject:factora1]){
                        [factorsOfA addObject:factora2];      
                    }
                    
                } //close  a % i == 0 if loop
            } //close for loop.
            NSNumber *factor = [[NSNumber alloc] init];
            for (int j = [factorsOfA count] -1; j>= 0 ; j--) {
                
                factor = [factorsOfA objectAtIndex:j];
                factorInt = [factor intValue];
                NSLog(@"%@", factorsOfA);
                
                if (bAbs % factorInt == 0 ) {
                    a = a/factorInt;
                    b = b/factorInt ;
                    NSLog(@"factorInt %d", factorInt);
                    break;
                    
                }
            }      
        }  
        if (a< 0) {
            factorInt *=-1;
            a *=-1;
            b *= -1;
            negativeFactor = YES;
        }

        
        if (a == 1 && factorInt == 1){
            if(b > 0){
                answerTextView.text = [NSString stringWithFormat:@" x(x + %d)" ,b];
            }else if (b < 0){
                answerTextView.text = [NSString stringWithFormat:@"x(x - %d)", -b];
            }else{
                answerTextView.text = [NSString stringWithFormat:@"Unfactorable."];
            }
            
        }
        else if (a == 1 && factorInt != 1){
            if(b > 0){
                answerTextView.text = [NSString stringWithFormat:@"%dx(x + %d)", factorInt ,b];
            }else if (b < 0){
                answerTextView.text = [NSString stringWithFormat:@"%dx(x - %d)",factorInt, -b];
            }else{
                answerTextView.text = [NSString stringWithFormat:@"Unfactorable"];
            }
        }else if(a != 1 && factorInt == 1){
            if(b > 0){
                answerTextView.text = [NSString stringWithFormat:@" x(%dx + %d)" ,a ,b];
            }else if (b < 0){
                answerTextView.text = [NSString stringWithFormat:@"x(%dx - %d)",a, -b];
            }else{
                answerTextView.text = [NSString stringWithFormat:@"Unfactorable."];
            }

        }else{
            if(b > 0){
                answerTextView.text = [NSString stringWithFormat:@"%dx(%dx + %d)",factorInt, a ,b];
            }else if (b < 0){
                answerTextView.text = [NSString stringWithFormat:@"%dx(%dx - %d)",factorInt,a, -b];
            }else{
                answerTextView.text = [NSString stringWithFormat:@"Unfactorable."];
            }
            
        }
        factorInt = 0;
        factorsOfA = nil;
    }
    else{  
        
        
        //get factors of A
        for (int i = 1; i <= aAbs; i++){
            
            if (aAbs % i == 0){
                NSNumber *factora1 = [[NSNumber alloc] initWithInt : i];
                NSNumber *factora2 = [[NSNumber alloc] initWithInt: (aAbs/i)];
                
                if(factora1 == factora2){
                    [factorsOfA addObject: factora1];
                    [factorsOfA addObject: factora2];
                }
                else if(![factorsOfA containsObject:factora1]){
                    [factorsOfA addObject: factora1];
                }else if(![factorsOfA containsObject:factora1]){
                    [factorsOfA addObject:factora2];      
                }
                
            } //close  a % i == 0 if loop
        } //close for loop.
        
        
        //get factors of C
        for (int l = 1; l <= cAbs; l++){
            if(cAbs % l == 0){
                NSNumber *factorc1 = [[NSNumber alloc] initWithInt: l];
                NSNumber *factorc2 = [[NSNumber alloc] initWithInt: (cAbs/l)];
                if(factorc1 == factorc2){
                    [factorsOfC addObject: factorc1];
                    [factorsOfC addObject: factorc2];
                }
                else if(![factorsOfC containsObject:factorc1]){
                    [factorsOfC addObject: factorc1];
                }else if(![factorsOfC containsObject:factorc1]){
                    [factorsOfC addObject:factorc2];      
                }
                
            } //close  c % l == 0 if loop
            
        } //close for loop.
        NSLog(@"%@", factorsOfA);
        NSLog(@"%@", factorsOfC);
        
        
        
        for (int j= 0; j < [factorsOfA count]/2; j++){
            if(firstOption == b || secondOption == b || thirdOption == b || fourthOption == b || fifthOption == b || sixthOption == b || seventhOption == b || eighthOption == b){
                break;
            }
            for (int k = 0; k < [factorsOfC count]/2 ; k++){
                NSLog(@"%d", [factorsOfA count]);
                NSLog(@"%d %d %d", a ,b ,c);
                NSNumber *aNow = [factorsOfA objectAtIndex:j];
                NSNumber *aNext = [factorsOfA objectAtIndex:(([factorsOfA count]-1) - j)];
                NSNumber *cNow = [factorsOfC objectAtIndex:k];
                NSNumber *cNext = [factorsOfC objectAtIndex:(([factorsOfC count] -1)- k)];
                
                a1 = [aNow intValue];
                a2 = [aNext intValue];
                c1 = [cNow intValue];
                c2 = [cNext intValue];
                
                if(c > 0 && b > 0){
                    //1st, fifth
                    firstOption = (a1* c1) + (a2 * c2);
                    fifthOption = (a2 *c1) + (a1 * c2);
                    if(firstOption == b || fifthOption ==b){
                        break;
                    }
                }else if (c > 0 && b < 0){
                    //fourth, eighth
                    fourthOption = (a1 * (-c1)) + (a2 * (-c2));
                    eighthOption = (a2*(-c1)) + (a1 * (-c2));
                    
                    
                    if(fourthOption == b || eighthOption == b){
                        ;                
                        break;
                        
                    }
                    
                    
                    
                    
                }else if (c < 0){
                    secondOption = (a1 * (-c1)) + (a2 * c2);
                    thirdOption = (a1 * c1) + (a2 * (-c2));
                    sixthOption = (a2 * (-c1)) + (a1 *c2);
                    seventhOption = (a2*c1) + (a1 * (-c2));
                    if (secondOption == b || thirdOption == b || sixthOption == b || seventhOption == b) {
                        break;
                    }
                    
                    
                }
            } //close k for loop.
            
        } // close j for loop.
        
        if (firstOption == b ) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(x + %d)", c2, c1];
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(%dx + %d)", c2,a2,c1];
                
            }else if(a2 == 1 && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(x + %d)",a1, c2, c1];
                
            }else 
                
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(%dx + %d)",a1, c2,a2,c1];
        }
        
        else if (secondOption == b) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(x - %d)", c2,c1];
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(%dx - %d)", c2,a2,c1];
            }else if(a2 == 1 && a1 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(x - %d)",a1, c2, c1];
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(%dx - %d)",a1, c2,a2,c1];
            
        }
        
        else if (thirdOption == b) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(x + %d)", c2, c1];
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(%dx + %d)", c2,a2,c1];
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(x + %d)",a1, c2, c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(%dx + %d)",a1, c2,a2,c1];
            
            
        }
        
        else if (fourthOption == b){
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(x - %d)", c2, c1];
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(%dx - %d)", c2,a2,c1];
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(x - %d)",a1, c2, c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(%dx - %d)",a1, c2,a2,c1];
            
        }
        
        else if (fifthOption == b) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(x + %d)", c2, c1];
                
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(x + %d)",a2, c2, c1];
                
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(%dx + %d)", c2,a1,c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(%dx + %d)",a2, c2,a1,c1];
            
        }
        
        
        
        else if (sixthOption == b) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(x - %d)", c2,c1];
                
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(x - %d)",a2, c2,c1];
                
                
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(x + %d)(%dx - %d)", c2,a1, c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx + %d)(%dx - %d)",a2, c2,a1,c1];
            
            
        }
        else if (seventhOption == b) {
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(x + %d)", c2,c1];
                
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(x + %d)",a2, c2, c1];
                
                
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(%dx + %d)", c2,a1,c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(%dx + %d)",a2, c2,a1,c1];
            
            
        }
        
        else if(eighthOption == b){
            if (a1 == 1 && a2 == 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(x - %d)", c2,c1];
                
                
            }else if (a1 == 1 && a2 != 1){
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(x - %d)",a2, c2, c1];
                
                
            }else if(a2 == 1  && a1!= 1){
                answerTextView.text = [NSString stringWithFormat:@"(x - %d)(%dx - %d)", c2,a1,c1];
                
                
            }else  
                answerTextView.text = [NSString stringWithFormat:@"(%dx - %d)(%dx - %d)",a2, c2,a1,c1];
            
            
        }
        
        else {
            answerTextView.text = [NSString stringWithFormat:@"Unfactorable."];	
            notFactored = YES;
        }
        if(!notFactored && negativeFactor){
            NSString *negativeFactorString = @"-1";
            negativeFactorString = [negativeFactorString stringByAppendingString:answerTextView.text ];
            answerTextView.text = [NSString stringWithFormat:@"%@", negativeFactorString];
        }
        a = [aTextField.text intValue];
        b = [bTextField.text intValue];
        c = [cTextField.text intValue];
        if (notFactored) {
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style> <p> To factor the following quadratic in standard form, we need to find the factors of the C value (constant term) and A value(x<sup>2</sup> coefficient) that will add up to the B value(x term's coefficient).</p><table><tr><td>%dx<sup>2</sup> + %dx + %d><p> The factors of A are:&plusmn; %@</p> <p> The factors of C are:&plusmn; %@</p><p> Since there are no factors of A and C that add up to B this quadratic is:</p> <table><tr><td class='answer'>%@</td></tr></table></html>",a,b,c, factorsOfA, factorsOfC, answerTextView.text];
            
        }else if (isBinomial){
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style> <p> Since the following quadratic is a binomial, we need to find the factor that is common between the two terms.</p></tr></table><p> The common factor is %dx</p> <p> Now we can factor out the common factor to get:</p> <table><tr><td class='answer'>%d</td></tr></table></html>",a,b] ;//],factorInt, answerTextView.text];
        }
        else{
            answerString = [NSString stringWithFormat:@"<html><style>td.space {width:40%%; text-align:right;} td.answer {border:solid 2px red; }</style> <p> To factor the following quadratic in standard form, we need to find the factors of the C value (constant term) and A value(x<sup>2</sup> coefficient) that will add up to the B value(x term's coefficient).</p><table><tr><td>%dx<sup>2</sup> + %dx + %d</td></tr></table><p> The factors of A are:&plusmn; %@</p> <p> The factors of C are:&plusmn; %@</p><p> Selecting the appropriate factors of A and C that add up to B we get:</p> <table><tr><td class='answer'>%@</td></tr></table></html>",a,b,c, factorsOfA, factorsOfC, answerTextView.text];
        }
        factorsOfA = nil;
        factorsOfC = nil;
        firstOption = 0;
        secondOption = 0;
        thirdOption = 0;
        fourthOption = 0;
        fifthOption =0;
        sixthOption = 0;
        seventhOption = 0;
        eighthOption = 0;
        
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [self.navigationItem setRightBarButtonItem: showWork animated:YES];    
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        mainDetailViewController.answerString = answerTextView.text;
        mainDetailViewController.solutionString = answerString;
        [mainDetailViewController.answerWebView setHidden:YES];

        [mainDetailViewController configureView];
        
    }
    [self createContentPages];
   
}

- (void) createContentPages
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.solutionCount++;
    if([appDelegate.summaryStringsArray count] >= 30){
        [appDelegate.summaryStringsArray removeObjectAtIndex:0];
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy hh:mm:ssa"];
    NSDate *now = [NSDate date];
    NSString *dateString = [dateFormat stringFromDate:now];
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPositiveFormat:@"###0.##"];
    
    NSString *aString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:a]];
    NSString *bString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:b]];
    NSString *cString = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:c]];
    
    
    NSString *contentString = [NSString 
                               stringWithFormat:@"<html><head></head><body><h2>%@ <br/>The Factors of the quadratic  %@x<sup>2</sup> + %@x + %@ are<br/> %@ </h2></body></html>",dateString, aString, bString, cString,answerTextView.text ];
    
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        //[summaryViewController.webViewString appendFormat:contentString];
        //[summaryViewController viewDidLoad];
        [appDelegate.summaryStringsArray addObject:contentString];
        //[appDelegate.summaryWebViewString appendFormat:contentString];
        //[summaryViewController configureView];
    }else{
        
        [appDelegate.summaryStringsArray addObject:contentString];
        [mainDetailViewController viewWillAppear:YES];    }
}

-(void)showWorkView{
    ShowWorkViewController *workView = [[ShowWorkViewController alloc] initWithNibName:@"ShowWorkViewController" bundle:nil];
    workView.html = answerString;
    [self.navigationController pushViewController:workView animated:YES];
    [workView.webView loadHTMLString:answerString baseURL:nil];
    
}



- (void)dealloc {
	/*[aTextField release];
	[bTextField release];
	[cTextField release];
    [super dealloc];*/
}

@end
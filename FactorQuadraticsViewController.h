//
//  FactorQuadraticsViewController.h
//  Algebralator
//
//  Created by Scott LaForest on 8/4/11.
//  Copyright 2011 Scott LaForest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MasterViewController.h"
#import <iAd/iAd.h>

@class DetailViewController;

@interface FactorQuadraticsViewController : UIViewController <UITextFieldDelegate>
{


@private
	UITextField *aTextField;
	UITextField *bTextField;
	UITextField *cTextField;
	UITextField *activeField;
    
	NSMutableArray *factorsOfA;
    NSMutableArray *factorsOfC;
    int checkfactora1;
     int checkfactora2;
    int checkfactorc1;
   int checkfactorc2;
    int a;
    int b;
   int c ;
     double xPlus;
     double xMin;
     double xPlusRound;
     double xMinRound;
     int aAbs;
    int bAbs;
     int cAbs;
    int factorInt;
     int firstOption;
     int secondOption;
     int thirdOption;
     int fourthOption;
     int fifthOption;
     int sixthOption;
     int seventhOption;
     int eighthOption ;
     int a1;
     int a2;
     int c1;
     int c2;
    double discriminant;
    NSString *answerString;

}
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) DetailViewController *mainDetailViewController;
@property (strong, nonatomic) DetailViewController *summaryViewController;


@property (nonatomic, retain) IBOutlet UITextField *aTextField;
@property (nonatomic, retain) IBOutlet UITextField *bTextField;
@property (nonatomic, retain) IBOutlet UITextField *cTextField;
@property (nonatomic, retain) IBOutlet UITextView *directionsTextView;
@property (nonatomic, retain) IBOutlet UITextView *answerTextView;
@property (nonatomic, retain) IBOutlet UINavigationBar *navigationBar;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *showWork;

-(void)showWorkView;
- (void) createContentPages;


- (IBAction) factorButtonPressed: (id) sender;
-(IBAction) textFieldDoneEditing: (id)sender;
-(IBAction)clearButtonPressed:(id)sender;

@end
